var async = require('async'),
    system = require('system'),
    sizes = [
        [320,  480],
        [480,  320],        
        [1024, 768],
        [768, 1024],        
        [1440, 900]
        //[640,  960], //2x
        //[640, 1136], //2x
        //[2048,1536], //2x
    ];

var userAgents = {
        "320x480":"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 Launcheck/0.1",
        "480x320":"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 Launcheck/0.1",        
        "1024x768": "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 Launcheck/0.1",
        "768x1024": "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 Launcheck/0.1",        
        "1440x900": "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36 Launcheck/0.1"
        //"640x960":"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25",
        //"640×1136":"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25",
        //"2048x1536": "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25",
};


var address = system.args[1];
var output = system.args[2];
var index = system.args[3];


function capture(index) {
    var page = require('webpage').create();

    page.viewportSize = {
        width: parseInt(sizes[index][0]),
        height: parseInt(sizes[index][1])
    };

    console.log("opening", address, "size",page.viewportSize.width,page.viewportSize.height);

    page.settings.userAgent =  userAgents[index];
    page.open(address, function (status) {
        console.log("open");
        if (status !== 'success') {
            console.log('Not Found');
        } else {

page.evaluate(function() {
  var style = document.createElement('style'),
      text = document.createTextNode('body { background: #fff }');
  style.setAttribute('type', 'text/css');
  style.appendChild(text);
  document.head.insertBefore(style, document.head.firstChild);
});


            /*No retina display in phantomjs by now
            switch(index){
                case 5:
                case 6:
                case 7:
                    page.evaluate(function () {
                        
                        document.body.style.webkitTransform = "scale(2)";
                        document.body.style.webkitTransformOrigin = "0% 0%";
                        
                        document.body.style.width = "50%";
                        window.devicePixelRatio = 2;
                    });
                    console.log("2x");
                break;            
            }
            */
    

            var filename = output + sizes[index][0] + 'x' + sizes[index][1] + '.jpg';
            var filepath = './temp/' + filename;
            console.log("Preparing to render");

            window.setTimeout(function () {
                page.clipRect = { left: 0, top: 0, width: sizes[index][0], height: sizes[index][1] };
                page.render(filepath,{ quality: 70 });
                console.log("Rendered and saved in ",filepath);
                window.setTimeout(function () {
                    phantom.exit();
                },300);                
            }, 200);
        }
        
    });
}
capture(index);
