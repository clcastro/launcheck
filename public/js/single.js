Handlebars.registerHelper("debug", function(optionalValue) {
 /* console.log("Current Context");
  console.log("====================");
  console.log(this);
 
  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }*/
});    

function SortByLoadTime(a, b){
  if(a.yahooYSlow && b.yahooYSlow){
   var at = a.yahooYSlow.score;
   var bt = b.yahooYSlow.score; 
  }else{
    var at = a.downloadTime;
    var bt = b.downloadTime; 
  }
  return ((at < bt) ? 1 : ((at > bt) ? -1 : 0));
}

pages.sort(SortByLoadTime);

Handlebars.registerHelper('thumbkey', function(shot) {  
  return (shot.key.split(".jpg")[0]+"thumb.jpg");
  //shot.key.split(".jpg")[0]+"thumb"+".jpg";
});
Handlebars.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});


Handlebars.registerHelper('urlpath', function(url) {  
  return (shot.key.split(".jpg")[0]+"thumb.jpg");  
});

var source   = $("#page-result-template").html();
var result_template = Handlebars.compile(source);    



$.each(pages,function(index,item){   
    item.index = index;    

    var shots = [];
    $.each(pages_screenshots,function(index,sh){        
        if(sh._pagecheck == item._id ){
            
            shots.push(sh);
        }
    });
    
    item.screenshots = shots;
    
    $page = $(result_template(item));
    $("#page-grid").append($page);        
    
});



var generalDatum = [
        {   
            key:"Load Time",
            values:[]
        },
        {
            key:"Page Size",
            values:[]
        },
        {
        key:"Total Page Size",
            values:[  ]
        },
       
    ];








var optimizationDatum = [
        { key:"Google PageSpeed Desktop Score", values:[ ] },
        { key:"Yahoo YSlow Score", values:[ ] },
        { key:"Google PageSpeed Mobile Score", values:[ ] }
        
    ]; 



if(pages){

    generalDatum = [
        { key:"Requests (x10) ", values:[ ] },
        { key:"Load Time (seconds)", values:[ ] },        
        { key:"Size (Mb)", values:[ ] }
    ];

    var optimizationDatum = [
        { key:"PageSpeed Desktop", values:[ ] },
        { key:"PageSpeed Mobile", values:[ ] },
        { key:"Yahoo YSlow Score", values:[ ] }        
    ]; 

    var validationDatum = [
        { key:"JS Errors", values:[ ] },
        { key:"HTML Errors", values:[ ] },
        { key:"CSS Errors", values:[ ] },
        { key:"CSS Warnings", values:[ ] }    
    ];
    
    var sumCSSErrors = 0;
    var sumCSSWarnings = 0;
    var sumHTMLErrors = 0;
    var sumJSErrors = 0;

    var minPS = 100;
    var minPSM = 100;
    var minYS = 100;

    $.each(pages,function(index,item){
        console.log(item);
        if(item.yahooYSlow){
            generalDatum[0].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.yahooYSlow.requests /10 });
            generalDatum[1].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.yahooYSlow.loadtime / 1000 });
            generalDatum[2].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.yahooYSlow.size / (1024*1024) });
        }
        else{
            generalDatum[0].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0  });
            generalDatum[1].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });
            generalDatum[2].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });   
        } 


        if(item.googlePageSpeedDesktop){
            if(minPS >= item.googlePageSpeedDesktop.score)
                minPS = item.googlePageSpeedDesktop.score
            optimizationDatum[0].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.googlePageSpeedDesktop.score }); 
        }
        else  optimizationDatum[0].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });

        if(item.googlePageSpeedMobile) {
            if(minPSM >= item.googlePageSpeedMobile.score)
                minPSM = item.googlePageSpeedMobile.score
            optimizationDatum[2].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.googlePageSpeedMobile.score });
        }
        else  optimizationDatum[2].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });

        if(item.yahooYSlow){ 
            if(minYS >= item.yahooYSlow.score)
                minYS = item.yahooYSlow.score
            optimizationDatum[1].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.yahooYSlow.score });
        }
        else  optimizationDatum[1].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });




        if(item.cssValidation && item.cssValidation.errorCount) 
        {
            sumCSSErrors += item.cssValidation.errorCount;
            sumCSSWarnings += item.cssValidation.warningCount;
            validationDatum[2].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.cssValidation.errorCount });
            validationDatum[3].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : item.cssValidation.warningCount });            
        }
        else{
            validationDatum[2].values.push({label:item.title +"(" + item.url + ")"+"(" + item.url + ")", x : index , id: item._id, y : 0,y0:0,y1:0 });
            validationDatum[3].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 ,y0:0,y1:0});
        }
        

        if(item.htmlValidation && item.htmlValidation.messages.length) 
        {
            sumHTMLErrors += item.htmlValidation.messages.length;
            validationDatum[1].values.push({label:item.title +"(" + item.url + ")", x  : index , id: item._id, y : item.htmlValidation.messages.length });            
        }
        else{
            validationDatum[1].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });            
        }

        if(item.jsErrors && item.jsErrors.count) 
        {
            sumJSErrors += item.jsErrors.count;
            validationDatum[0].values.push({label:item.title +"(" + item.url + ")" , x : index , id: item._id, y : item.jsErrors.count });            
        }
        else{
            validationDatum[0].values.push({label:item.title +"(" + item.url + ")", x : index , id: item._id, y : 0 });            
        }

        
        $(".single-item.errors-score-min-pagespeed big").text(minPS);
        
        if(minPS < 60)
         $(".single-item.errors-score-min-pagespeed").addClass("score-error");
        else if(minPS >= 60 && minPS < 90 ) 
          $(".single-item.errors-score-min-pagespeed").addClass("score-warning");
        else if(minPS >= 90 )
            $(".single-item.errors-score-min-pagespeed").addClass("score-excellent");
        
        $(".single-item.errors-score-min-yslow big").text(minYS);

        if(minYS < 60) $(".single-item.errors-score-min-yslow ").addClass("score-error");
        else if(minYS >= 60 && minYS < 90 ) $(".errors-score-min-yslow").addClass("score-warning");
        else if(minYS >= 90 ) $(".single-item.errors-score-min-yslow").addClass("score-excellent");

        $(".single-item.errors-score-min-pagespeed-mobile big").text(minPSM);

        if(minPSM < 60) $(".single-item.errors-score-min-pagespeed-mobile").addClass("score-error");
        else if(minPSM >= 60 && minPSM < 90 ) $(".single-item.errors-score-min-pagespeed-mobile").addClass("score-warning");
        else if(minPSM >= 90 ) $(".single-item.errors-score-min-pagespeed-mobile").addClass("score-excellent");
 

        $(".single-item.errors-score-js big").text(sumJSErrors> 100 ? "99+":sumJSErrors);
        $(".single-item.errors-score-html big").text(sumHTMLErrors> 100 ? "99+":sumHTMLErrors);
        $(".single-item.errors-score-css-e big").text(sumCSSErrors> 100 ? "99+":sumCSSErrors);
        $(".single-item.errors-score-css-w big").text(sumCSSWarnings> 100 ? "99+":sumCSSWarnings);
        if(sumJSErrors == 0) $(".single-item.errors-score-js").removeClass("errors-score").addClass("max-score");
    });
    
}
    


$(document).ready(function(){
   


    $(".page-states-menu a ").bind("click",function(ev){


        $(".page-states-menu li.current").removeClass("current");
        $(this).parents("li").addClass("current");   
        $("#page-grid").attr("class","").addClass( $(this).attr("rel") );
    });

    $(".more-info >li > h4 > a").bind("click",function(ev){
        ev.preventDefault();
        console.log("hi");
        if($(this).parents("li").is(".show")){
            $(this).parents("li").removeClass("show");
        }
        else{
            $(this).parents("li").addClass("show");   
        }

    });

    $(".details > li > a ").bind("click",function(ev){
        ev.preventDefault();
        if($(this).parents("li").is(".details-current")){
            $(this).parents("li").removeClass("details-current");
        }
        else{
            $(".details > li.details-current").removeClass("details-current");
            $(this).parents("li").addClass("details-current");   
        }
        /***********

         if($(this).parents("li").is(".details-site") && $(".details-site").data("loaded") != "yes"  ){

            nv.addGraph({
              generate: function() {     
                var chart = nv.models.multiBarChart()                
                    .showLegend(true)
                    .stacked(false)                         
                    .x(function(d) { return d.label });
                            
                                 
                chart.yAxis.tickFormat(d3.format(',.1f'));
                d3.select('#chart-general svg').datum(generalDatum).transition().duration(500).call(chart);    

                $(".details-site").data("loaded","yes") ;
              }
            });
   
        }


        if($(this).parents("li").is(".details-validation") && $(".details-validation").data("loaded") != "yes"  ){

            nv.addGraph({
              generate: function() {     
                var chart2 = nv.models.multiBarChart()                
                    .showLegend(true)
                    .stacked(true)     
                    
                    .x(function(d) { return d.label });
                            
                                 
                chart2.yAxis.tickFormat(d3.format(',.1f'));
                d3.select('#chart-validation svg').datum(validationDatum).transition().duration(500).call(chart2);    

                $(".details-validation").data("loaded","yes") ;
              }
            });
   
        }

         if($(this).parents("li").is(".details-performance") && $(".details-performance").data("loaded") != "yes"  ){

                nv.addGraph({
                  generate: function() {     
                    var chart3 = nv.models.multiBarChart()                
                        .showLegend(true)           
                        .stacked(false)     
                        .x(function(d) { return d.label });
                        
                             
                    chart3.yAxis.tickFormat(d3.format(',.1f'));
                    d3.select('#chart-performance svg').datum(optimizationDatum).transition().duration(500).call(chart3);    

                    $(".details-performance").data("loaded","yes") ;
                  }
                });
        }

****************************/

       
    });

                nv.addGraph({
              generate: function() {     
                var chart2 = nv.models.multiBarChart()                
                    .showLegend(true)
                    .stacked(true)     
                    .showXAxis(false)
                    .x(function(d) { return d.label });
                            
                                 
                chart2.yAxis.tickFormat(d3.format(',.1f'));
                d3.select('#chart-validation svg').datum(validationDatum).transition().duration(500).call(chart2);    

                $(".details-validation").data("loaded","yes") ;
              }
            });
   

/*            nv.addGraph({
              generate: function() {     
                var chart = nv.models.multiBarChart()                
                   // .showLegend(true)
                    .stacked(false)                         
                    .x(function(d) { return d.label });
                chart.yAxis.tickFormat(d3.format(',.1f'));
                d3.select('#chart-general svg').datum(generalDatum).transition().duration(500).call(chart);
                $(".details-site").data("loaded","yes") ;
              }
            });
*/
            nv.addGraph({
              generate: function() {     
                var chart3 = nv.models.multiBarChart()                
                    //.showLegend(true)           
                    .stacked(false)  
                    .showXAxis(false)

                    .x(function(d) { return d.label });                                             
                chart3.yAxis.tickFormat(d3.format(',.1f')).scale(100);

                d3.select('#chart-performance svg').datum(optimizationDatum).transition().duration(500).call(chart3);    
                $(".details-performance").data("loaded","yes") ;
              }
            });
        


});


