var passport = require('passport'),
    Account = require('./models/account'),
    Domain = require('./models/domain'),
    DomainCheck = require('./models/domaincheck'),
    Screenshot = require('./models/screenshot'),
    Task = require('./models/task'),
    http = require('follow-redirects').http;

var https = require('follow-redirects').https;
    $ = require('jquery'),
    AWS = require('aws-sdk'),
    url = require('url');
    kue = require('kue'),
    jobs = kue.createQueue();

var logentries = require('node-logentries');
var log = logentries.logger({
  token:'1e8d763f-b943-4c65-9c7b-2bca21a20279'
});

var FROOTCONFIG = require('config');
AWS.config.loadFromPath('./config/config-aws.json');




module.exports = function (app) {

    app.get('/auth/google', passport.authenticate('google'))
    app.get('/auth/google/return', 
    passport.authenticate('google', { successRedirect: '/dashboard',
                                    failureRedirect: '/login' }));

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }) );
    app.get('/auth/facebook/callback', 
    passport.authenticate('facebook', { successRedirect: '/dashboard',
                                    failureRedirect: '/login' }));

    app.get('/', function (req, res) {
        log.info("New visit",getUserIp(req));
        //The user is logged in, show the last user checks
        if(req.user){
            DomainCheck
                .find({_uid:req.user._id})
                .populate("_domain")
                .populate("checks")
                .limit(8)
                .sort('-created')
                .exec(function(err,checks){                                
                     res.render('index.jade', { 'layout':false,checks:checks, user : req.user });
                });
        }
        else{
        //The user is not logged in, show the last 10 public checks
         DomainCheck
                .find({_uid:{$exists:false}})
                .populate("_domain")
                .populate("checks")
                .limit(8)
                .sort('-created')
                .exec(function(err,checks){                                
                     res.render('index.jade', {  'layout':false,checks:checks, user : req.user });
                });
        }


    });
    
    //This is kind of empty right now
    app.get('/register', function(req, res) {
        res.render('register', { user:req.user });
    });

    //Basic registration, email and password 
    //[TODO] (we have to check that is the email, and show any errors)
    app.post('/register', function(req, res) {
        Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
            if (err) {
                return res.render('register', { account : account ,err:err });
            }
            res.redirect('/');
        });
    });

    app.post('/check', function(req, res) {

        
        var baseurl = req.body.url;        
        if( baseurl.indexOf("http://") < 0 && baseurl.indexOf("https://") < 0 ) {
            baseurl = "http://" + baseurl ;
        }        
        var checkurl = url.parse(baseurl);        
        log.info("debug",baseurl);
        var requester = http;        
        if( baseurl.indexOf("https://") >= 0 ) {            
            requester = https;
        }
        console.log(checkurl);
        var nreq = requester.request(checkurl, function(httpres) {

                //var checkurl = url.parse(httpres.req._headers.host + httpres.req.path); 
                if(httpres.statusCode == 200){
                    /**  The url is ok so we continue **/                    
                    Domain.findOne({protocol:checkurl.protocol,hostname:checkurl.hostname,path:checkurl.path}, function (err, dmn) {
                        if (err) log.alert("Error searching the domain on (/check) [url|",checkurl,"]" ,err); 
                        if(!dmn){
                            //The domain does not exists for this account
                            //Create the domain with the default check config and add it to the queue  
                            var domain = new Domain({ created: new Date(), hasRunningCheck:false, protocol: checkurl.protocol, hostname: checkurl.hostname, port: checkurl.port || 80, path: checkurl.path,  testsCount: 0 });
                            domain.save(function (err) {
                                      if (err) return log.alert("Error saving the domain on (/check) [url|",baseurl,"]",err);

                                      var dcheck = new DomainCheck({  });
                                      dcheck._domain = domain;
                                      dcheck.created = new Date();
                                      dcheck.running = true;
                                      if(req.user){
                                                dcheck._uid = req.user._id;
                                       }

                                      dcheck.save(function(error,dc){
                                        if (err) return log.alert("Error saving the domaincheck on (/check) [url|",baseurl,"]",err);         
                                        domain.checks.push( dcheck );
                                        domain.hasRunningCheck = true;
                                        domain.save(function (err) {
                                           if (err) return log.alert("Error updating the domain on (/check) [url|",baseurl,"]",err);         
                                                                                                                                    
                                            var jobmeta =  {
                                                 title: 'Crawl '+ checkurl.hostname+''+ checkurl.path,
                                                 checkid: dcheck._id                                             
                                            },
                                            jobmetaTimeOut =  {
                                                 title: 'Timeout '+ checkurl.hostname+''+ checkurl.path,
                                                 checkid: dcheck._id                                             
                                            };
                                            if(req.user){
                                                jobmeta.uid = req.user._id;
                                                jobmetaTimeOut.uid = req.user._id;
                                            }
                                            //Logged in users have higher priority
                                            //[TODO] in the future paid user will have "high" and "critical" priority or have their own kue
                                            var priority = 'low'; 
                                            if(req.user) priority = 'medium';
                                            var job = jobs.create(FROOTCONFIG.minions.crawler,jobmeta).priority(priority).save(function(err){
                                                    if (err) return log.critical("Error saving the kue ("+FROOTCONFIG.minions.crawler+") on (/check) [url|",baseurl,"]",err); 
                                            });
                                            jobs.create(FROOTCONFIG.minions.timeout,jobmetaTimeOut).priority(priority).delay(FROOTCONFIG.minions.timeoutTime).save(function(err){
                                                if (err) return log.critical("Error saving the kue ("+FROOTCONFIG.minions.timeout+") with delay ("+FROOTCONFIG.minions.timeoutTime+") on (/check) [url|",baseurl,"]",err); 

                                            });
                                            //[TODO we could link the progress to the client interface with socket.io]
                                            /***
                                            job.
                                            on('progress', function(progress){
                                                  process.stdout.write('\r  job #' + job.id + ' ' + progress + '% complete');
                                            }).
                                            on('complete', function(){
                                              
                                            });
                                            **/
                                            res.redirect('/check/'+dc._id);                                            
                                        });
                                });                                      
                            });


                        }else{                            
                            if(dmn.hasRunningCheck){
                                log.alert("Domain has a running check [url|",baseurl,"]");
                                res.redirect('/checkerrors/isactive/0'); //We tell that the domain has a check already running in this account
                            }
                            else{
                                //Create the check with the default check config and add it to the queue                                  
                                  var dcheck = new DomainCheck({  });
                                  dcheck._domain = dmn._id;
                                  dcheck.created = new Date();
                                  dcheck.running = true;
                                  if(req.user){
                                        dcheck._uid = req.user._id;                                        
                                  }


                                  dcheck.save(function(error,dc){
                                    if (err) return log.alert("Error saving the domaincheck on (/check) [url|",baseurl,"]",err);
                                    dmn.checks.push( dcheck );
                                    dmn.hasRunningCheck = true;
                                    dmn.save(function (err) {
                                       if (err) return log.alert("Error updating the domain on (/check) [url|",baseurl,"]",err);
                                        console.log("suceess");
                                        // En este momento debieramos encolar el crawling 
                                        console.log("We queue the crawler");
                                        var jobmeta =  {
                                                 title: 'Crawl '+ checkurl.hostname+''+ checkurl.path,
                                                 checkid: dcheck._id                                             
                                            },
                                            jobmetaTimeOut =  {
                                                 title: 'Timeout '+ checkurl.hostname+''+ checkurl.path,
                                                 checkid: dcheck._id                                             
                                            };
                                            if(req.user){
                                                jobmeta.uid = req.user._id;
                                                jobmetaTimeOut.uid = req.user._id;
                                            }
                                            //Logged in users have higher priority
                                            //[TODO] in the future paid user will have "high" and "critical" priority or have their own kue
                                             var priority = 'low'; 
                                            if(req.user) priority = 'medium';
                                            var job = jobs.create(FROOTCONFIG.minions.crawler,jobmeta).priority(priority).save(function(err){
                                                    if (err) return log.critical("Error saving the kue ("+FROOTCONFIG.minions.crawler+") on (/check) [url|",baseurl,"]",err); 
                                            });
                                            jobs.create(FROOTCONFIG.minions.timeout,jobmetaTimeOut).priority(priority).delay(FROOTCONFIG.minions.timeoutTime).save(function(err){
                                                if (err) return log.critical("Error saving the kue ("+FROOTCONFIG.minions.timeout+") with delay ("+FROOTCONFIG.minions.timeoutTime+") on (/check) [url|",baseurl,"]",err); 

                                            });

                                        res.redirect('/check/'+dc._id);

                                    });

                                  });                                      

                            }
                        }

                    });                    
                    /**********************************/
                }
                else{
                    log.alert("Error accesing the domain in (/check) [url|",baseurl,"] [code:",httpres.statusCode,"]");
                    res.redirect('/checkerrors/httperror/'+httpres.statusCode); //We tell that the domain has a check already running in this account                
                }
                httpres.on('end',function(){  });
        });
        nreq.on('error', function(e) {
            log.alert("Error accesing the domain in (/check) [url|",baseurl,"] "+ e.message,e);          
            res.redirect('/checkerrors/httperror/'+404);
        });
        nreq.end();
        
    });

    app.get('/check', function(req, res) {
        res.render('single',  {layout:'layoutsingle.jade' });
    });


    app.get('/checkerrors/:type/:code', function(req, res) {
        res.render('checkerrors',  {type:req.params.type,code:req.params.code });
    });

    app.get('/check/:id', function(req, res) {
        var checkid = req.params.id;
        DomainCheck
            .findOne({_id:checkid})
            .populate("_domain")
            .populate("pages")
           .populate("files")
            //.populate("pagechecks pages")
            .exec(function (err, dcheck) {
              if (err) return console.warn(err);   

              if(dcheck.running)                                
              {                
                if(dcheck.pendingCrawl)
                {
                    res.render('single-crawling',  { check:dcheck,status:'crawling',tasks:{total:0,succeded:0,failed:0},layout:'layoutsingle.jade', user : req.user});
                }
                else{
                    Task
                    .find({checkid:dcheck._id })
                    .select('status')
                    .exec(function (err, tasks) {
                        var total = 0;
                        var succeded = 0;
                        var failed = 0;
                        for(i=0;i<tasks.length;i++)
                        {
                            total++;
                            if(tasks[i].status == 'failed') failed++;
                            if(tasks[i].status == 'succeded') succeded++;
                        }               
                        res.render('single-crawling',  { check:dcheck,status:'processing',tasks:{total:total,succeded:succeded,failed:failed}, layout:'layoutsingle.jade', user : req.user});
                    });
                }
              }                
              else{
                  Screenshot
                    .find({_domaincheck:checkid})
                    .exec(function(err,shots){                    
                        if(err){
                            console.log("error check not found");
                            res.redirect('/');
                        }
                        else{
                            if(dcheck.pendingCrawl)
                            	res.render('single-crawling',  { check:dcheck,screenshots:shots ,layout:'layoutsingle.jade', user : req.user});
                        	else
                        		res.render('single',  { check:dcheck,screenshots:shots ,layout:'layoutsingle.jade', user : req.user});
                        }                    

                  });
              }
        });
    });



    app.get('/login', function(req, res) {
        console.log(req.user);
        res.render('login', { user : req.user });
    });

    app.get('/check/:id/status', function(req, res) {
        var checkid = req.params.id;
        DomainCheck
        .findOne({_id:checkid})
        .exec(function (err, dcheck) {
            res.writeHead(200, { 'Content-Type': 'application/json'});
            if(!dcheck){
                return res.end(JSON.stringify({ 'status' : 'notfound' }));
            }
            else if(dcheck.pendingCrawl)
            {
                return res.end(JSON.stringify({ 'status' : 'crawling' }));
            }
            else if(!dcheck.running)
            {
                return res.end(JSON.stringify({ 'status' : 'completed' }));   
            }
            else{
                Task
                .find({checkid:checkid })
                .select('status')
                .exec(function (err, tasks) {
                    
                    if(err){
                        res.end(JSON.stringify({ 'status' : 'error' }));
                    }
                    else if(!tasks)
                    {
                        res.end(JSON.stringify({ 'status' : 'notfound' }));
                    }
                    else{
                        var total = 0;
                        var succeded = 0;
                        var failed = 0;
                        for(i=0;i<tasks.length;i++)
                        {
                            total++;
                            if(tasks[i].status == 'failed') failed++;
                            if(tasks[i].status == 'succeded') succeded++;
                        }       
                        res.end(JSON.stringify({
                            'status' : 'progress',
                            'total': total,
                            'progress': failed+succeded
                        }));                        
                    }
                });
            }

        });
        
        
    });


    app.post('/login', passport.authenticate('local'), function(req, res) {
        res.redirect('/dashboard');
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    /** Account related paths **/
    app.get('/account/dashboard', function(req, res) {       
        if(req.user && req.user._id){
            res.render('dashboard', { user : req.user });    
        }
        else{
            res.redirect('/');     
        }        
    });


    app.get('/account/profile', function(req, res) {       
        if(req.user && req.user._id){
            res.render('profile', { user : req.user });    
        }
        else{
            res.redirect('/');     
        }        
    });
    app.get('/account/profile/update', function(req, res) {       
        if(req.user && req.user._id){
            res.render('profile-update', { user : req.user });    
        }
        else{
            res.redirect('/');     
        }        
    });

    app.get('/account/all-checks', function(req, res) {       
        if(req.user && req.user._id){
            res.render('allchecks', { user : req.user });    
        }
        else{
            res.redirect('/');     
        }        
    });

};


function getUserIp(req){
    return req.headers['x-forwarded-for'] || 
     req.connection.remoteAddress || 
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;
}