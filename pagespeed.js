
var mongoose = require('mongoose');
var http = require('http');
var cuco = require('./lib/cuco');
var kue = require('kue'),
    jobs = kue.createQueue();
/*MODELS*/
var PageCheck = require('./models/pagecheck');
var Task = require('./models/task');

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);    

function optimizationProcess(job,done,item,index){
		
	Task
	.findOne({name:"pagespeed-web",pageid:job.data.pageid,checkid:job.data.checkid })	
	.exec(function (err, task) {
			
		cuco.getGooglePageSpeedDesktop(job.data.pageid,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedDesktop");
					if(err){

				 		task.log = [err];
						task.status = 'failed';
						task.save(function(){ doMobile();	 });							

						
					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  		pageStats: pagespeed.pageStats,
					  		formattedResults: pagespeed.formattedResults.ruleResults
					  	};


					  	item.googlePageSpeedDesktop = obj;
					  	item.save(function(err){
					  		if(err){
					  			task.log = [err];
								task.status = 'failed';
								task.save(function(){ doMobile();	 });	
					  		}else{
					  			task.status = 'succeded';
								task.save(function(){ doMobile(); });							
					  		}
					  		
					  	})
					  	
					}
		});
	});

	function doMobile()
	{
		Task
		.findOne({name:"pagespeed-mobile",pageid:job.data.pageid,checkid:job.data.checkid })	
		.exec(function (err, task) {
			cuco.getGooglePageSpeedMobile(job.data.pageid,item.url,function(err,pagespeed){
						console.log("getGooglePageSpeedMobile");
						if(err){
							task.log = [err];
							task.status = 'failed';
							task.save(function(){ finishUp(job,done,item);	 });	
						}else{					
					      	var obj = {
						  		completedOn: new Date(),
						  		size: pagespeed.pageStats.totalRequestBytes,
						  		score: pagespeed.score,
						  		requests: pagespeed.pageStats.numberResources,	
						  		pageStats: pagespeed.pageStats,
					  			formattedResults: pagespeed.formattedResults.ruleResults  		
						  	};

						  	item.googlePageSpeedMobile = obj;
						  	item.save(function(){
						  		if(err){
						  			task.log = [err];
									task.status = 'failed';
									task.save(function(){ finishUp(job,done,item);	 });	
						  		}else{
						  			task.status = 'succeded';
									task.save(function(){ finishUp(job,done,item);	 });
						  		}
						  	})

						  	//TODO: FALTA GUARDAR El file en s3
						}
			});
		});
	}
	
}


function finishUp(job,done,page){
		
		//Here we check if we completed all tasks from this domaincheck, if that is the case we call the finish-him minion
		Task
		.find({checkid:job.data.checkid })
		.select('status')
		.exec(function (err, tasks) {
			if(err){
				 console.log("error al obtener tareas",err);
			}
			if(tasks.length == 0)
			{
				 console.log("no hay tasks")
			}
			else{
				var total = 0;
				var succeded = 0;
				var failed = 0;
				for(i=0;i<tasks.length;i++)
				{
					total++;
					if(tasks[i].status == 'failed') failed++;
					if(tasks[i].status == 'succeded') succeded++;
				}
				if( failed+succeded == total){
					jobs.create(FROOTCONFIG.minions.finishhim,{checkid:job.data.checkid,title:"finish him "+job.data.checkid}).priority('high').save(function(){});			
					console.log("finish him");
				}else{
					console.log("not finish him, incomplete yet ",succeded+failed," from ",total);
				}
			}
			done();
		});
	
}


jobs.process(FROOTCONFIG.minions.pagespeed,3, function(job, done){
	console.log(job.data);
	PageCheck
	.findOne({_id:job.data.pageid,_domaincheck:job.data.checkid })	
	.exec(function (err, page) {
	  if(!page) 
	  {
	  	console.log("PAGE NOT FOUND");
	  }
	  if (err){
	   console.warn("Error loading the page",err);					 					  	  
	  }	  
	  try{
	  	optimizationProcess(job,done,page);

	  }catch(err){
	  	console.log("ERRRORRR");
	  	console.log(err);
	  	done(err);
	  }
	});
});

/*
Stand alone mode, we use this for testing when the Kue is not working
setTimeout(nextTask, 3000);
function nextTask()
{

	Task
	.findOne({name:"pagespeed-web",status:'created' })	
	.sort('-created')
	.exec(function (err, task) {
		if(task){
			var jobmeta =  {
		         title: 'pagespeed TEST',
		         checkid: task.checkid,
		         pageid: task.pageid
		    };
		    console.log(jobmeta);
			job = jobs.create(FROOTCONFIG.minions.pagespeed,jobmeta).priority('normal').save(function(err){			
				
			});
		}else{
			console.log("no more tasks dude");
			setTimeout(nextTask, 1000);
		}
	});
}*/