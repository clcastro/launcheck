var AWS = require('aws-sdk');

var mongoose = require('mongoose');
var imagemagick = require('imagemagick-native');
var fs = require('fs');
var googleapis = require('googleapis');
var sys = require('sys')
var exec = require('child_process').exec;
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');

var kue = require('kue'),
    jobs = kue.createQueue();


/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Task = require('./models/task');

var child;
var checkid = '';
var domaincheckid = '';

var https = require('https'),
    strategy = 'desktop';


var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);





function validate(job,done,dcheck,page){


	(function processFile(itm) {

	var headers = { 
	    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0'
	};
		
	
	Task
	.findOne({name:"css-validation",pageid:job.data.pageid,checkid:job.data.checkid })	
	.exec(function (err, task) {
		
		  if (err){
		   console.warn("Error finding the page",err);					 					  	  
		  }
		  if(task.status == 'created')
		  {
		  	
		  	http.get({
			    host: 'jigsaw.w3.org', 
			    path: '/css-validator/validator?output=json&uri='+itm.url,
			    headers:headers

			   }, function(res) {
			      
			      var str = "";
			      res.on('data', function(d) {
			        str+= d.toString();
			      });
			      res.on('end',function(){	      	
			      	try{
			      		var result = JSON.parse(str);	      		
			      	}
			      	catch(e){
			      		console.warn("Error parsing cssvalidation",e,str);
			      		task.log = ["Error parsing css validation",e];
						task.status = 'failed';
						task.save(function(){ goHTML();	 });
			      		return;
			      	}
			      	
			      	if(result.cssvalidation){	      		
				      	var obj = {
					  		completedOn: new Date(),
					  		csslevel: result.cssvalidation.csslevel,
					  		errorCount: result.cssvalidation.result.errorcount,
					  		warningCount: result.cssvalidation.result.warningcount,
					  		valid: result.cssvalidation.validity
					  	};
					  	itm.cssValidation = obj;
					} 
					itm.save(function(err, itemoso){
						if(err){  

							 console.log("Error saving html validation",itemoso.url,err);
							 task.log = [err];
							 task.status = 'failed';
							 task.save(function(){ goHTML();	 });							

						}else{
							task.status = 'succeded';
							task.save(function(){ goHTML();	 });							
						}						
						
						
					})
			      

			      });
			}).on('error', function(e) {
			    console.error("error loading css validator",itm.url,e);	  
			  	task.log = [err];
				task.status = 'failed';
				task.save(function(){ goHTML();	 });
			});	
		  }

		  
	});

	
	function goHTML()
	{
		Task
		.findOne({name:"html-validation",pageid:job.data.pageid,checkid:job.data.checkid })	
		.exec(function (err, task) {
			 if (err){
			   console.warn("Error finding the task",err);					 					  	  
			 }
			 else if(!task)
			 {
			 	console.warn("Error finding the task");	
			 }
			

			
			http.get({
			    host: 'validator.w3.org', 
			    path: '/check?output=json&uri='+itm.url,
			    headers:headers
			    }, function(res) {
			      console.log(res.statusCode);
			      var str = "";
			      res.on('data', function(d) {
			        str+= d.toString();
			      });
			      res.on('end',function(){	      	
			      	
			      	try{
			      		var result = JSON.parse(str);
			      		
			      	}
			      	catch(e){		      		
						task.log = [err];
						task.status = 'failed';
						task.save(function(){ finishUp(job,done,dcheck,page);  });
						console.log("error parsing item");
						//AQUI VA UN [FinishUp]

						return ;
			      	}	  

			      	
			      	if(result.url){
				      	var obj = {
				      		valid: result.messages.length == 0,
					  		completedOn: new Date(),
					  		messagesCount: result.messages.length,
					  		messages: result.messages
					  	};
					  	itm.htmlValidation = obj;
					} 
					itm.save(function(err, itemoso){
						if(err){ 
							task.log = [err];
							task.status = 'failed';
							task.save(function(){ finishUp(job,done,dcheck,page);  });
							return;
						 }					
						else{						
							task.status = 'succeded';
							task.save(function(){ finishUp(job,done,dcheck,page);  });
							return;
						}	
						
					})
				    
			      });
			}).on('error', function(e) {
			   task.log = [err];
			   task.status = 'failed';
			   task.save(function(){ finishUp(job,done,dcheck,page);  });
			   return;
			});	
				
		});
	}
				    
	})(page);
	
}


function finishUp(job,done,dcheck,page){
		
		//Here we check if we completed all tasks from this domaincheck, if that is the case we call the finish-him minion
		Task
		.find({checkid:job.data.checkid })
		.select('status')
		.exec(function (err, tasks) {
			if(err){
				 console.log("error al obtener tareas",err);
			}
			if(tasks.length == 0)
			{
				 console.log("no hay tasks")
			}
			else{
				var total = 0;
				var succeded = 0;
				var failed = 0;
				for(i=0;i<tasks.length;i++)
				{
					total++;
					if(tasks[i].status == 'failed') failed++;
					if(tasks[i].status == 'succeded') succeded++;
				}
				if( failed+succeded >= total){
					jobs.create(FROOTCONFIG.minions.finishhim,{checkid:job.data.checkid,title:"finish him "+job.data.checkid}).priority('high').save(function(){});			
					console.log("finish him");
				}else{
					console.log("not finish him, incomplete yet ",succeded+failed," from ",total);
				}
			}
			done();
		});
	
}


function slugifyUrl(text){
    text=  text.toString().toLowerCase();
    text = text.replace("http://", "");    
    text = text.replace(/\//gi, "-");
    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');
    

    text = text.replace(/\s/gi, "-");
    return text;
}


jobs.process(FROOTCONFIG.minions.validator, 3, function(job, done){
	

	PageCheck
	.findOne({_id:job.data.pageid,_domaincheck:job.data.checkid })
	.populate("_domaincheck")
	.exec(function (err, page) {
	  if(!page) 
	  {
	  	console.log("PAGE NOT FOUND");
	  }
	  if (err){
	   console.warn("Error loading the page",err);					 					  	  
	  }	  
	  try{
	  	validate(job,done,page._domaincheck,page,0);
	  }catch(err){
	  	console.log("ERRRORRR");
	  	console.log(err);
	  	done(err);
	  }
	});
});

