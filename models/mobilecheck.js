var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MobileCheck = new Schema({
    created: Date,
    completed: { type:Boolean , default:false },    
    time: Number,
    mobileOkScore: Number,
    viewport: String
    /*And more data*/
});

module.exports = mongoose.model('MobileCheck', MobileCheck);