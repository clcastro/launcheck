var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GooglePageSpeed = new Schema({
    created: Date,
    completed: { type:Boolean , default:false },    
    time: Number,
    score: Number
    /*And more data*/
});

module.exports = mongoose.model('GooglePageSpeed', GooglePageSpeed);