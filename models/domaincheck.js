var mongoose = require('mongoose'),
	Domain = require('./domain'),	
    DomainCheckConfig = require('./domaincheckconfig'), 
	DomainCheckFile = require('./domaincheckfile'),
	PageCheck = require('./pagecheck'),
    Schema = mongoose.Schema;



var DomainCheck = new Schema({
    _domain:  [{ type: Schema.ObjectId, ref: 'Domain' }],
    _uid : { type: Schema.ObjectId, ref: 'Account' },
	config: [DomainCheckConfig],
    files:[{  type: Schema.ObjectId, ref: 'DomainCheckFile'  }],
    pages:[{  type: Schema.ObjectId, ref: 'PageCheck'  }],
    logs:Array,
    created: Date,
    completed: Date,    
    running: {type:Boolean,default:false},
    pendingCrawl: {type:Boolean,default:true},
    pendingValidation: {type:Boolean,default:true},
    pendingBrowsing: {type:Boolean,default:true},
    pendingCuconsole: {type:Boolean,default:true},
    sucessfull: Boolean,
    crawlSuccesfull : Boolean,
    crawlErrors: Array,

    crawlCompletedOn: Date, 
    validationCompletedOn: Date,
    browsingCompletedOn: Date,
    cuconsoleCompletedOn: Date,
    brokenLinksNumber:Number,
    removeWWW:Boolean,
    hasSitemap:Boolean,
    sitemap: String, /*By now  String*/
    hasRobotsTXT: Boolean,
    robotsTXT: String, /*By now String */

    hasHumansTXT: Boolean,
    humansTXT: String, /*By now String */

    hasFavicon: Boolean,
    faviconIsIcon: Boolean,
 
    /* fast data */
    totalFiles : {type: Number,default:0},
    totalSize:  {type: Number,default:0},
    totalPages : {type: Number,default:0},
    totalPagesChecked: {type: Number,default:0},
    totalSize: {type: Number,default:0},
    totalHtmlErrors: {type: Number,default:0},
    totalCSSErrors: {type: Number,default:0}
});


module.exports = mongoose.model('DomainCheck', DomainCheck);