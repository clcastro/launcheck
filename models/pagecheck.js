var mongoose = require('mongoose'),
	
	DomainCheck = require('./domaincheck'),
	//HtmlValidation = require('./htmlvalidation'),
	//CssValidation = require('./cssvalidation'),
	//JsErrors = require('./jserrors'),	

	//GooglePageSpeed = require('./googlepagespeed'),
	//YahooYSlow = require('./yahooyslow'),

    Schema = mongoose.Schema;

var PageCheckSchema = new Schema({
	_domain : { type: Schema.ObjectId, ref: 'Domain' },
	_domaincheck : { type: Schema.ObjectId, ref: 'DomainCheck' },
	url : String,
	//METADATA
    httpStatus:Number,   	
    requestLatency: Number,
    requestTime: Number,
    downloadTime: Number,
    contentLength: Number,
    contentType: String,

    title:String,
    favicon: String,
    apple_icon: String,
    metas:Array,
    win8:{
    	applicationName:String,
    	tileColor: String
    },
	//MOBILE
	mobile : {		
    	mobileOkScore: Number,
    	viewport: String
	},
	//SEO
	seo:{
		author: String,
		description: String,
		generator: String,
		keywords: String,
		robots: String,
 		refresh: String
	},

	//SOCIAL
	facebook:{
		site_name: String,
		title: String,
		description: String,
		image: String,
		type: String,
		url: String,
		fb_admins: String
	},
	twitter:{
		card: String,
		url: String,	
		title: String,
		description: String,
		image: String,
		site: String,
		creator: String
	},
	//HTML5
	html5: 
	{
		is: Boolean,
	},
	//VALIDATION
	htmlValidation: {
		completedOn: Date,    
    	messagesCount: Number,
    	valid: Boolean,
    	messages: Array
	},
	cssValidation: {
		completedOn: Date,    
    	csslevel: String,
    	valid: Boolean,
    	errorCount: Number,
    	warningCount: Number
	},
	//jsErrors: [JsErrors],
	jsErrors:{
		count: Number,
		messages:Array
	},
	consoleMessages:{
		count: Number,
		messages:Array
	},
	har:{
		created:Date,
		bucket: String,
    	key: String,
    	zone: String
	},

	//PERFORMACE
	googlePageSpeedDesktop: {
		completedOn: Date,
		size: Number,
		score: Number,
		requests: Number,
		pageStats: Object,
		formattedResults:Object
	},	
	googlePageSpeedMobile: {
		completedOn: Date,
		size: Number,
		score: Number,
		requests: Number,
		pageStats: Object,
		formattedResults:Object
	},
	yahooYSlow: {
		completedOn: Date,
		size: Number,
		score: Number,
		requests: Number,
		loadtime: Number

	}
});


module.exports = mongoose.model('PageCheck', PageCheckSchema);