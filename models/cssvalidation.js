var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CssValidation = new Schema({
    created: Date,
    completed: { type:Boolean , default:false },    
    time: Number,
    errors: Number,
    valid: { type:Boolean , default:false }
});

module.exports = mongoose.model('CssValidation', CssValidation);