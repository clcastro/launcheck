var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Header = new Schema({
   status:{code:Number,text:String},
   date: Date,
   server: String,
   cacheControl: String,
   contentEncoding: String,
   contentLength: Number,
   contentType: String,
   expires: Date,
   lastModified: Date,
   pragma: String,
   WWWAuthenticate: String
});

module.exports = mongoose.model('Header', Header);