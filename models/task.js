var mongoose = require('mongoose'),
	DomainCheck = require('./domaincheck'),
	PageCheck = require('./pagecheck'),
    Schema = mongoose.Schema;

var TaskSchema = new Schema({    
    checkid : { type: Schema.ObjectId, ref: 'DomainCheck' },
    pageid:  { type: Schema.ObjectId, ref: 'PageCheck' },
    created: Date, 
    name: String,
    args: Object,
    status: String,    
    log: Array
});
module.exports = mongoose.model('Task', TaskSchema);