var mongoose = require('mongoose'),
	DomainCheck = require('./domaincheck'),
	PageCheck = require('./pagecheck'),
    Schema = mongoose.Schema;

var ScreenshotSchema = new Schema({    
    _domaincheck : { type: Schema.ObjectId, ref: 'DomainCheck' },
    _pagecheck:  { type: Schema.ObjectId, ref: 'PageCheck' },
    created: Date, 
    bucket: String,
    key: String,
    keyThumb:  String,
    width: Number,
    height: Number,
    realWidth: Number,
    realHeight: Number,    
    thumbnailRealWidth:Number,
    thumbnailRealHeight:Number,
    fileSize: Number,
    thumbnailFileSize: Number
});
module.exports = mongoose.model('Screenshot', ScreenshotSchema);