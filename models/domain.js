var mongoose = require('mongoose'),
	Account = require('./account'),
	DomainCheck = require('./domaincheck'),
    Schema = mongoose.Schema;


var Domain = new Schema({
	_account: { type: Schema.ObjectId, ref: 'Account' },
	created: Date,
	protocol: String,
	hostname: String,
	port: Number,
	path: String,
	testsCount: Number,
	hasRunningCheck: {type:Boolean,default:false},
	checks:  [{ type: Schema.ObjectId, ref: 'DomainCheck' }]

});


module.exports = mongoose.model('Domain', Domain);