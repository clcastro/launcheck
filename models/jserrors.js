var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var JsErrors = new Schema({
    created: Date,
    completed: { type:Boolean , default:false },    
    time: Number,
    errors: Number
    /*And more data*/
});

module.exports = mongoose.model('JsErrors', JsErrors);