var mongoose = require('mongoose'),	
	DomainCheck = require('./domaincheck'),
    Schema = mongoose.Schema;


var DomainCheckFile = new Schema({
	_domaincheck : { type: Schema.ObjectId, ref: 'DomainCheck' },
   url: String,
   created: Date,

   httpStatus:Number,   	
   requestLatency: Number,
   requestTime: Number,
   downloadTime: Number,
   contentLength: Number,
   contentType: String,

   headers:{ 
	   cacheControl: String,
	   contentEncoding: String,
	   contentLength: Number,
	   contentType: String,
	   expires: Date,
	   lastModified: Date,
	   pragma: String,
	   WWWAuthenticate: String
   }
	   
});

module.exports = mongoose.model('DomainCheckFile', DomainCheckFile);