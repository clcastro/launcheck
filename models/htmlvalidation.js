var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var HtmlValidationSchema = new Schema({
    valid: { type:Boolean , default:false },    
    created: Date,
    completed: { type:Boolean , default:false },    
    time: Number,
    errors: Number
});

module.exports = mongoose.model('HtmlValidation', HtmlValidationSchema);