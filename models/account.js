var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

var AccountSchema = new Schema({    
//  email: String, // username is email
    openid: String,
    fbid: String,    
    nickname: String,
    birthdate: Date,
    displayName : String,
    name: { familyName :String, givenName :String, middleName :String},
    photos : [{value:String}]

});

AccountSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', AccountSchema);