var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Tag = new Schema({
    key: String,
    value: String    
});
module.exports = mongoose.model('Tag', Tag);