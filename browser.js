var AWS = require('aws-sdk');
var mongoose = require('mongoose');
var googleapis = require('googleapis');
var sys = require('sys')
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var cuco = require('./lib/cuco');
var fs = require('fs');

var kue = require('kue'),
    jobs = kue.createQueue();

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);
AWS.config.loadFromPath('./config/config-aws.json');

/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Screenshot = require('./models/screenshot');
var Task = require('./models/task');

var bucket = FROOTCONFIG.aws.bucket;

var child;
var checkid = '';
var domaincheckid = '';

var s3 = new AWS.S3();
var processed = [];

function finishUp(job,done,page){		
		//Here we check if we completed all tasks from this domaincheck, if that is the case we call the finish-him minion
		Task
		.find({checkid:job.data.checkid })
		.select('status')
		.exec(function (err, tasks) {
			if(err){
				 console.log("error al obtener tareas",err);
			}
			if(tasks.length == 0)
			{
				 console.log("no hay tasks")
			}
			else{
				var total = 0;
				var succeded = 0;
				var failed = 0;
				for(i=0;i<tasks.length;i++)
				{
					total++;
					if(tasks[i].status == 'failed') failed++;
					if(tasks[i].status == 'succeded') succeded++;
				}
				if( failed+succeded == total){
					jobs.create(FROOTCONFIG.minions.finishhim,{checkid:job.data.checkid,title:"finish him "+job.data.checkid}).priority('high').save(function(){});			
					console.log("finish him");
				}else{
					console.log("not finish him, incomplete yet ",succeded+failed," from ",total);
				}
			}
			done();			
			//nextTask();
		}); 
}

function createScreenShots(job,done,item){

	var sizes = ["screenshot-320-480","screenshot-480-320","screenshot-1024-768","screenshot-768-1024","screenshot-1440-900"];


	console.log(job.data);
	function next(index)
	{
		try{
			if(!sizes[index])
			{
				return finishUp(job,done,item);
			}
			console.log(index,"finding task");
			Task
			.findOne({name:sizes[index],pageid:job.data.pageid,checkid:job.data.checkid })	
			.exec(function (err, task) {			
				

				function processsss(err,indx,img,thumb,imgData,thumbData,filesize,filename,filepath){					
					
						if(err){
							console.log("getScreenshot with errors");
							task.log = ["Error creating screenshot",err];
							task.status = 'failed';
							if(filepath){
								fs.exists(filepath, function (exists) {
								  if(exists)
								  	fs.unlinkSync(filepath);
								});								
							}

							return task.save(function(){ next(index+1);	 });
						}else{										
							console.log("getScreenshot	without errors");
								var filekey = filename+".jpg";
								var thumbkey = filename+"thumb.jpg";
								s3.client.putObject({
								    Bucket: bucket,
								    Key: filekey,
								    Body: img,
								    ContentType: 'image/jpeg'
								  },function (err1) {							  	
								    if(err1){ console.error("Error saving the image",err1); }
									 console.log("Uploaded Image ",indx)
									 s3.client.putObject({
									    Bucket: bucket,
									    Key: thumbkey,
									    Body: thumb,
									    ContentType: 'image/jpeg'
									  },function (err2) {
											if(err2){
												console.error("Error saving the thumb",err2);
											}else{
												console.log("saved thumb",indx);
												Screenshot.create(
												{
													_domaincheck : item._domaincheck,
												    _pagecheck:  item._id,
												    created: new Date(), 
												    bucket: bucket,
												    key: filekey,
												    keyThumb: thumbkey,
												    width: cuco.screenshotSizes[indx][0],
												    height: cuco.screenshotSizes[indx][1],
												    realWidth: imgData.width,
												    realHeight: imgData.height,    
												    thumbnailRealWidth:thumbData.width,
												    thumbnailRealHeight:thumbData.height,
												    fileSize: filesize												    
												}, function (err3) {
													if(err3) return console.error("Error saving the screenshot");
													console.log("saved screenshot ",task.name,item.url);
													fs.unlinkSync(filepath);
													if(err1 || err2 || err3)
													{
														task.log = ["Error SAVING screenshot",err1,err2,err3];
														task.status = 'failed';
														return task.save(function(){ next(1+index);	 });
													}
													else{
														task.status = 'succeded';
														task.save(function(){ next(1+index); });							

													}												
												});

											}
									  });
									
								  });										
						}
					}



				if (err){
				   console.log("Error finding the page",err);					 					  	  
				}
				if(task.status == 'created')
				{
					console.log("calling getScreenshot");					
					cuco.getScreenshot(job.data.pageid,item.url,index,processsss);			
				}
				else{
					console.log("[task-already-processed]");
					next(index +1);
				}


				



			});
		}catch(err)
		{
			console.log("Error");
			console.log(err);
		}
	}
	next(0);
}


jobs.process(FROOTCONFIG.minions.browser,4, function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	PageCheck
	.findOne({_id:job.data.pageid,_domaincheck:job.data.checkid })	
	.exec(function (err, page) {
	  if(!page) 
	  {
	  	console.log("PAGE NOT FOUND");
	  }
	  else if (err){
	   console.warn("Error loading the page",err);					 					  	  
	  }	  
	  else{
		  try{
		  	console.log(
		  		)
		  	createScreenShots(job,done,page);

		  }catch(err){
		  	console.log("ERRRORRR");
		  	console.log(err);
		  	done(err);
		  }
		}
	});
});



/*
Stand alone mode, we use this for testing when the Kue is not working*/
/*setTimeout(nextTask, 3000);
function nextTask()
{

	Task
	.findOne({$or:[ {'name': 'screenshot-320-480'}, {'name': 'screenshot-1024-768'},{name:'screenshot-1440-900'}],status:'created' })	
	.sort('-created')
	.exec(function (err, task) {
		if(task){
			var jobmeta =  {
		         title: 'pagespeed TEST',
		         checkid: task.checkid,
		         pageid: task.pageid
		    };
		    console.log(jobmeta);
			job = jobs.create(FROOTCONFIG.minions.browser,jobmeta).priority('normal').save(function(err){			
				
			});
		}else{
			console.log("no more tasks dude");
			setTimeout(nextTask, 1000);
		}
	});
}*/