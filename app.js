var path = require('path'),
    express = require('express'),
    http = require('http'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    GoogleStrategy = require('passport-google').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy;

var FROOTCONFIG = require('config');
var logentries = require('node-logentries');
var log = logentries.logger({
  token:'1e8d763f-b943-4c65-9c7b-2bca21a20279'
});


var app = express();

// Configuration
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('view options', { layout: false });

//app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(express.cookieParser('your secret here'));
app.use(express.session());

app.use(passport.initialize());
app.use(passport.session());

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.locals.pretty = true;
});

app.configure('production', function(){
    app.use(express.errorHandler());
    app.locals.pretty = true;
});

// Configure passport
var Account = require('./models/account');


passport.use(new LocalStrategy(Account.authenticate()));
passport.use(new GoogleStrategy({
    returnURL: FROOTCONFIG.general.baseurl + FROOTCONFIG.google.returnUrl,
    realm: FROOTCONFIG.general.baseurl
  },
  function(identifier, profile, done) {    
    console.log(profile.emails[0].value,identifier,profile);    
    Account.findOne({username: profile.emails[0].value }, function(err,account){
        if(err){
            console.log("error findOne",err);
            done(err, account); 
        }
        else if(!account)
        {
            var account = Account({ username:profile.emails[0].value,  name: {familyName: profile.name.familyName,givenName: profile.name.givenName } });            
            account.save(function(err,acc){
                done(err, acc);                
            });
        }else{
           done(err, account); 
        }  
    })
  }
));


passport.use(new FacebookStrategy({
    clientID: FROOTCONFIG.facebook.clientID,
    clientSecret: FROOTCONFIG.facebook.clientSecret,
    callbackURL: FROOTCONFIG.general.baseurl + FROOTCONFIG.facebook.callbackUrl
  },
  function(accessToken, refreshToken, profile, done) {
    Account.findOne({username: profile.emails[0].value }, function(err,account){
        if(err){
            console.log("error findOne",err);
            done(err, account); 
        }
        else if(!account)
        {
            var account = Account({ fbid : profile.id , name: profile.name, username: profile.emails[0].value });            
            account.save(function(err,acc){
                done(err, acc);                
            });
        }else{
           done(err, account); 
        }  
    });

  }

));


passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// Connect mongoose
mongoose.connect(FROOTCONFIG.db.mongo);

// Setup routes
require('./routes')(app);

http.createServer(app).listen(FROOTCONFIG.general.port, '0.0.0.0', function() {
    console.log("Express server listening on %s:%d in %s mode", '0.0.0.0', FROOTCONFIG.general.port, app.settings.env);
    log.alert("[app-restart] 0.0.0.0:",FROOTCONFIG.general.port," in ",app.settings.env," mode");
});


var kue = require('kue');
kue.app.listen(FROOTCONFIG.general.kueport);