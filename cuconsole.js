var AWS = require('aws-sdk');
var mongoose = require('mongoose');
var googleapis = require('googleapis');
var sys = require('sys')
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var cuco = require('./lib/cuco');

var fs = require('fs');
var kue = require('kue'),
    jobs = kue.createQueue();

/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Task = require('./models/task');

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);
AWS.config.loadFromPath('./config/config-aws.json');
var bucket = FROOTCONFIG.aws.bucket;

var s3 = new AWS.S3();
var processed = [];

function optimizationProcess(job,done,item)
{

	Task
	.findOne({name:"js-errors-and-console",pageid:job.data.pageid,checkid:job.data.checkid })	
	.exec(function (err, task) {

		cuco.getErrorsAndConsole(job.data.pageid,item.url,function(err,res,filename){
			console.log("Filename:",filename)
			console.log("getErrorsAndConsole");
			if(err){
				console.log("Error saving js and console");
				console.log(err);
				var consoleResults = null;
				var jserrors = null;
				console.log("NOT SAVING JSErrors");
				task.log = [err];
				task.status = 'failed';
				task.save(function(){ 
						if(filename) fs.unlink(filename, function(err){   })
						doHar();	
				});
			}else{
				var consoleResults = res.console;
				var jserrors = res.jserrors;	
				var obj = {
			  		count: jserrors.length,
			  		messages: jserrors			  		
			  	};
			  	var obj2 = {
			  		count: consoleResults.length,
			  		messages: consoleResults			  		
			  	};
			  	item.jsErrors = obj;
			  	item.consoleMessages = obj2;
			  	item.save(function(){
			  		console.log("Saved JSErrors");
			  		
			  		task.status = 'succeded';
					task.save(function(){ 
						fs.unlinkSync(filename);
						delete res;
						doHar();
					});							
			  	});
			}				

			
		});		
	});	

	function doHar()
	{

		Task
		.findOne({name:"create-and-save-har",pageid:job.data.pageid,checkid:job.data.checkid })	
		.exec(function (err, task) {
			cuco.getHar(job.data.pageid,item.url,function(err,filename){		
				console.log("getHar",filename);
				if(err)
				{
					task.log = [err];
					task.status = 'failed';
					task.save(function(){ 						
						doYSlow(filename);	
					});
				}
				else{
					console.log("har created");
					var buffer = fs.readFileSync(filename);
					var filekey = job.data.pageid + '-har.json';
					console.log("saving Har");
					s3.client.putObject({
						    Bucket: bucket,
						    Key: filekey,
						    Body: buffer,
						    ContentType: 'application/json'
					},function (err) {
						if(err){
						 console.error("Error uploading the HAR to S3",err); 
							task.log = [err];
							task.status = 'failed';
							task.save(function(){ 						
								doYSlow(filename);	
							});
						}else{				
							console.log('har saved');
							item.har.created = new Date();
							item.har.bucket = bucket;
							item.har.filekey = filekey;
							item.save(function(){
								task.status = 'succeded';
								task.save(function(){ 					
									delete buffer;
									doYSlow(filename);
								});								
							})
							
						}
					
					});
						
					
				}				
				
			});
		});
	}

	function doYSlow(filename)
	{

		Task
		.findOne({name:"yslow",pageid:job.data.pageid,checkid:job.data.checkid })	
		.exec(function (err, task) {
			cuco.getYslowWithHar(job.data.pageid,item.url,filename,function(err,yslow,filen){					
				if(err){
					console.log("BROWSER: Error with yslow",err);
					task.log = [err];
					task.status = 'failed';
					task.save(function(){ 						
						if(filename) fs.unlink(filen, function(err){   })
						finishUp(job,done,item);
					});										
				}else{
					console.log("CuCONSOLE: getYslowWithHar");
					var obj = {
				  		completedOn: new Date(),
				  		size: yslow.w,
				  		score: yslow.o,
				  		requests: yslow.r,
				  		loadtime: yslow.lt
				  	};
				  	item.yahooYSlow = obj;
				  	item.save(function(){				  								
						task.status = 'succeded';
						task.save(function(){ 					
							fs.unlink(filen, function(err){   })
							fs.unlink(filename, function(err){   })
							finishUp(job,done,item);
						});												
				  	});
				}					
			});
		});
	}
  

  


}

function finishUp(job,done,page){
		
		//Here we check if we completed all tasks from this domaincheck, if that is the case we call the finish-him minion
		Task
		.find({checkid:job.data.checkid })
		.select('status')
		.exec(function (err, tasks) {
			if(err){
				 console.log("error al obtener tareas",err);
			}
			if(tasks.length == 0)
			{
				 console.log("no hay tasks")
			}
			else{
				var total = 0;
				var succeded = 0;
				var failed = 0;
				for(i=0;i<tasks.length;i++)
				{
					total++;
					if(tasks[i].status == 'failed') failed++;
					if(tasks[i].status == 'succeded') succeded++;
				}
				if( failed+succeded == total){
					jobs.create(FROOTCONFIG.minions.finishhim,{checkid:job.data.checkid,title:"finish him "+job.data.checkid}).priority('high').save(function(){});			
					console.log("finish him");
				}else{
					console.log("not finish him, incomplete yet ",succeded+failed," from ",total);
				}
			}
			done();
			//nextTask();
			
		});
	
  
	}

jobs.process(FROOTCONFIG.minions.cuconsole,3, function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	PageCheck
	.findOne({_id:job.data.pageid,_domaincheck:job.data.checkid })	
	.exec(function (err, page) {
	  if(!page) 
	  {
	  	console.log("PAGE NOT FOUND");
	  }
	  if (err){
	   console.warn("Error loading the page",err);					 					  	  
	  }	  
	  try{
	  	optimizationProcess(job,done,page);

	  }catch(err){
	  	console.log("ERRRORRR");
	  	console.log(err);
	  	done(err);
	  }
	});
});



/*
Stand alone mode, we use this for testing when the Kue is not working*/
/*setTimeout(nextTask, 3000);
function nextTask()
{

	Task
	.findOne({name:"create-and-save-har",status:'created' })	
	.sort('-created')
	.exec(function (err, task) {
		if(task){
			var jobmeta =  {
		         title: 'pagespeed TEST',
		         checkid: task.checkid,
		         pageid: task.pageid
		    };
		    console.log(jobmeta);
			job = jobs.create(FROOTCONFIG.minions.cuconsole,jobmeta).priority('normal').save(function(err){			
				
			});
		}else{
			console.log("no more tasks dude");
			setTimeout(nextTask, 1000);
		}
	});
}*/