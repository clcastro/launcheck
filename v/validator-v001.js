var AWS = require('aws-sdk');

var mongoose = require('mongoose');
var imagemagick = require('imagemagick-native');
var fs = require('fs');
var googleapis = require('googleapis');
var sys = require('sys')
var exec = require('child_process').exec;
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');


/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Screenshot = require('./models/screenshot');

var bucket = 'launcheck001';


var child;
var checkid = '';
var domaincheckid = '';



var https = require('https'),
    key = 'KEY',
    url = 'URL',
    strategy = 'desktop';


mongoose.connect('mongodb://admin:loremipsumpassword@paulo.mongohq.com:10047/v001');
AWS.config.loadFromPath('./config.json');
var sqs = new AWS.SQS().client;
var s3 = new AWS.S3();
var crawlQueueUrl 		= "https://sqs.us-west-2.amazonaws.com/076753025882/crawl";
var browsingQueueUrl	= "https://sqs.us-west-2.amazonaws.com/076753025882/browsing";
var validationQueueUrl  = "https://sqs.us-west-2.amazonaws.com/076753025882/validation";


/*

*/
/*

*/



     function SQSReceiveMessage(){ 
     	

        sqs.receiveMessage( { QueueUrl: validationQueueUrl, MaxNumberOfMessages: 1,WaitTimeSeconds:20,VisibilityTimeout:15 }, function (err, job) {
                if (err) {
                    console.log(err);  
                    SQSReceiveMessage();                      
                    return;
                }

                if (!job.Messages || job.Messages.length === 0) {            		
            		SQSReceiveMessage();
                    return;
                }

                // Handle the message we pulled off the queue.
                var handle = job.Messages[0].ReceiptHandle,
                	body = null;
                
                try { // a JSON string message body is accepted.
                    body = JSON.parse( job.Messages[0].Body );
                } catch(e) {
                    if (e instanceof SyntaxError) {                		                    	
                        body = JSON.parse( new Buffer(job.Messages[0].Body, 'base64').toString( 'binary' ) );
                    } else {
                        throw e;
                    }
                }
                if(body.checkid)
                {
                	var pageCheckData = [];
                	var domainCheckFileData = [];

                	console.log("checkid:",body.checkid);
                	DomainCheck
                	.findOne({_id:body.checkid})
                	.populate("pages")
                	.exec(function (err, dcheck) {
					  if (err) return console.warn(index,"Error loading the pages",err);					 					  
					  checkid = body.checkid; 
					  domaincheckid = dcheck._id; 
					  validate(handle,dcheck,dcheck.pages,0);

					});
					
                }
                
        });

	}

SQSReceiveMessage();
function validate(handle,dcheck,pages,index){


	if(pages.length-1 < index)
	{
		console.log("Validation complete");

		dcheck.pendingValidation = false;
		if(dcheck.running && !dcheck.pendingBrowsing){
			dcheck.running = false;
			Domain
            	.findOne({_id:dcheck._domain})                	
            	.exec(function (err, domain) {
				  if (err) return console.warn(err);					 
				  domain.hasRunningCheck = false;
				  domain.save(function(){ 
				  	console.log("Domain marked as hasRunningCheck:false");
				  });
				});
		}
		dcheck.save(function(error,res){
			sqs.deleteMessage({QueueUrl : validationQueueUrl, ReceiptHandle: handle},function(error,item){
	  			if(error) return console.warn(index,"Error removing the crawl item from queue");	  			
	  			console.log(index,"The VALIDATION SQS item was removed");
	  			return SQSReceiveMessage();
	  		});	
		});

			
	}

	var item = pages[index];	
	if(!item){
		return setTimeout(function(){ SQSReceiveMessage(); }, 1000);
	}

	(function processFile(itm) {

	var headers = { 
	    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0'
	};
		

		http.get({
	    host: 'jigsaw.w3.org', 
	    path: '/css-validator/validator?output=json&uri='+itm.url,
	    headers:headers

	    }, function(res) {
	      
	      var str = "";
	      res.on('data', function(d) {
	        str+= d.toString();
	      });
	      res.on('end',function(){	      	
	      	try{
	      		var result = JSON.parse(str);	      		
	      	}
	      	catch(e){
	      		console.warn("Error parsing cssvalidation",e,str);
	      		return;
	      	}
	      	
	      	if(result.cssvalidation){	      		
		      	var obj = {
			  		completedOn: new Date(),
			  		csslevel: result.cssvalidation.csslevel,
			  		errorCount: result.cssvalidation.result.errorcount,
			  		warningCount: result.cssvalidation.result.warningcount,
			  		valid: result.cssvalidation.validity
			  	};
			  	itm.cssValidation = obj;
			} 
			itm.save(function(err, itemoso){
				if(err){  return console.log(index,"Error saving html validation",itemoso.url,err); }
				//console.log("css",itemoso);
		

				console.log(index,"Saved cssvalidation",itemoso.url);				
			})
	      

	      });
	}).on('error', function(e) {
	  console.error(index,"error loading css validator",itm.url,e);	  
	});	

	http.get({
		    host: 'validator.w3.org', 
		    path: '/check?output=json&uri='+itm.url,
		    headers:headers
		    }, function(res) {
		      console.log(res.statusCode);
		      var str = "";
		      res.on('data', function(d) {
		        str+= d.toString();
		      });
		      res.on('end',function(){	      	
		      	
		      	try{
		      		var result = JSON.parse(str);
		      		
		      	}
		      	catch(e){		      		
		      		validate(handle,dcheck,pages,++index); return console.log(index,"Error parsing html validation");
		      		return;
		      	}	  

		      	
		      	if(result.url){
			      	var obj = {
			      		valid: result.messages.length == 0,
				  		completedOn: new Date(),
				  		messagesCount: result.messages.length,
				  		messages: result.messages
				  	};
				  	itm.htmlValidation = obj;
				} 
				itm.save(function(err, itemoso){
					if(err){ validate(handle,dcheck,pages,++index); return console.log(index,"Error saving html validation",err); }					
					console.log(index,"Saved htmlvalidation",itemoso.url);
					setTimeout(function(){  validate(handle,dcheck,pages,index+1);   },1500);
				})
			    
		      });
		}).on('error', function(e) {
		  console.error(index,"error loading html validator",itm.url,e);
		  setTimeout(function(){  validate(handle,dcheck,pages,index+1);   },1500);
		  
		});	
			
				    
		})(item);
}





function slugifyUrl(text){
    text=  text.toString().toLowerCase();
    text = text.replace("http://", "");    
    text = text.replace(/\//gi, "-");
    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');
    

    text = text.replace(/\s/gi, "-");
    return text;
}
