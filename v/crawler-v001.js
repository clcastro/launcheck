var AWS = require('aws-sdk');
var mongoose = require('mongoose');
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var check4 = require('./lib/check4');


var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var parseString = require('xml2js').parseString;

var limit = 10;

mongoose.connect('mongodb://admin:loremipsumpassword@paulo.mongohq.com:10047/v001');

AWS.config.loadFromPath('./config.json');
var sqs = new AWS.SQS().client;
var crawlQueueUrl 		= "https://sqs.us-west-2.amazonaws.com/076753025882/crawl";
var browsingQueueUrl	= "https://sqs.us-west-2.amazonaws.com/076753025882/browsing";
var validationQueueUrl  = "https://sqs.us-west-2.amazonaws.com/076753025882/validation";

     function SQSReceiveMessage(){ 
     	

        sqs.receiveMessage( { QueueUrl: crawlQueueUrl, MaxNumberOfMessages: 1,WaitTimeSeconds:20,VisibilityTimeout:15 }, function (err, job) {
                if (err) {
                        console.log(err);  
                        SQSReceiveMessage();                      
                        return;
                }

                if (!job.Messages || job.Messages.length === 0) {
                		console.log("empty messages")
                		SQSReceiveMessage();
                        return;
                } 

                // Handle the message we pulled off the queue.
                var handle = job.Messages[0].ReceiptHandle,
                	body = null;
                

                try { // a JSON string message body is accepted.
                        body = JSON.parse( job.Messages[0].Body );
                } catch(e) {
                        if (e instanceof SyntaxError) {
                
                                body = JSON.parse( new Buffer(job.Messages[0].Body, 'base64').toString( 'binary' ) );
                        } else {
                                throw e;
                        }
                }
                

                if(body.checkid)
                {

                	var pageCheckData = [];
                	var domainCheckFileData = [];



                	console.log("We have a checkid, lets find the domaincheck");
                	DomainCheck
                	.findOne({_id:body.checkid})
                	.populate("_domain")
                	.exec(function (err, dcheck) {
					  if (err) return console.warn(err);
					  /*********************************/
					var urlobj = {hostname:dcheck._domain[0].hostname,port:dcheck._domain[0].port,path:dcheck._domain[0].path};  
					check4.sitemap( urlobj, function(err,smap){  
						console.log("sitemap",smap);
						check4.robots_txt( urlobj, function(err,robots){  
							
							check4.humans_txt( urlobj, function(err,humans){  
								check4.favicon( urlobj, function(err,isfavicon){  
									var rdata = {sitemap:smap,robots:robots,humans:humans,favicon:isfavicon};
									processDomainCheck(handle,dcheck,rdata);
									
								});	
							});	
						});
					});
				
						


						
					  
					});
					
                }


                
        });

	}


	function processDomainCheck(handle,dcheck,rdata){
				
					var urls = rdata.sitemap;
					

					var pageCheckData = [];
                	var domainCheckFileData = [];
                	var sitemapFound = false;
					 var checkurl = dcheck._domain[0].protocol + "//" + dcheck._domain[0].hostname + ( (dcheck._domain[0].port == 80 ||  !dcheck._domain[0].port )?"":(":"+dcheck._domain[0].port)  ) + dcheck._domain[0].path;
					  console.log("Checking: ",checkurl)

					  var crawler = Crawler.crawl(checkurl);					  
					  if(urls && urls.length > 0)
					  {
					  	sitemapFound = true;
					  	x = 0;
					  	urls.forEach(function(item) {
					  	 	if(x > limit) return;	
					  	   console.log(x,limit,"adding url",item.loc[0]);
			               crawler.queueURL(item.loc[0]);
			               x++;
			              			               
				        });
					  }
					  
					  crawler.addFetchCondition( function(parsedURL) {
			                  return !parsedURL.path.match(/\.(pdf|png|otf|jpg|gif|jpeg|svg|eot|woff|ttf|htc|js)$/i);
			          });
						
					  crawler			  					  
					  .on("fetchcomplete",function(queueItem, responseBuffer , response ){ 
					  	var url = queueItem.url;
						console.log("fetched:",url);

						if(queueItem.stateData  && queueItem.stateData.contentType &&  queueItem.stateData.contentType.match(/text\/html/gi) ){
							if(pageCheckData.length + 1 > limit){
				            	console.log("stopping crawl at the begining, total:",pageCheckData.length," maximum: ",limit);				            	
				            	return;
				            }									

						    var html = responseBuffer.toString(); 
							var data = {};
							try{
						        var $html = $(html);
					    	}catch(e){					    		
					    		console.log("Error parsing HTML",url);
					    		return;
					    	}

					    	data.html5 = {is: html.indexOf("<!DOCTYPE html>") > -1 };
				            data.title = $html.find("head title").text();                                    //sacamos el titulo
				            data.favicon = $html.find('link[rel~="icon"]').attr("href");                   //sacamos el favicon
				            $aicons = $html.find('link[rel="apple-touch-icon"],link[rel="apple-touch-icon-precomposed"]');
				            data.apple_icon = $aicons.length > 0 ? $($aicons[0]).attr("href"):'';				            				            
				            var gaRegExp = /_gaq.push\(\['_setAccount'\s*,\s*'(UA-\w*-\w)'\]\s*\)\s*;/gi; 
				            var match  = html.match(gaRegExp);
				            var ga = match && match[1];
				            if( ga ) data.ga = ga; 
				            
				      
				            var og = {};
				            var tc = {};
				            var info = {};
				            var seo = {};
				            var mobile = {};
				            var win8 ={};


				            $html.find("head meta").each(function(){
				                 $meta = $(this);
				                 var content = $meta.attr("content");    
				                 var property = $meta.attr("property")+"";
				                 var name = $meta.attr("name")+"";
				                 var equiv = $meta.attr("http-equiv")+"";

				                 switch( property.toLowerCase() )
				                 { 
				                
				                    case "og:site_name": og.site_name = content; break;  
				                    case "og:title":     og.title = content; break;
				                    case "og:description":     og.description = content; break;
				                    case "og:image":     og.image = content; break;
				                    case "og:type":      og.type = content; break;
				                    case "og:url":       og.url = content; break;
				                    case "fb:admins":    og.fb_admins = content; break;

				                    case "twitter:card":         tc.card = content; break;
				                    case "twitter:url":          tc.url = content; break;
				                    case "twitter:title":        tc.title = content; break;
				                    case "twitter:description":  tc.description = content; break;
				                    case "twitter:image":        tc.image = content; break;
				                    case "twitter:site":         tc.site = content; break;
				                    case "twitter:creator":      tc.creator = content; break;

				                 }

				                 switch( name.toLowerCase() )
				                 {
				                    case "author":       seo.author = content; break;
				                    case "description":  seo.description = content; break;
				                    case "generator":    seo.generator = content; break;
				                    case "keywords":     seo.keywords = content; break;
				                    case "robots":     seo.robots = content; break;
				                    case "generator":     seo.generator = content; break;
				                    case "viewport":             mobile.viewport = content; break;   
				                    case "application-name": win8.applicationName = content; break;
				                    case "msapplication-TileColor": win8.tileColor = content; break;
				                    
				                 }

				                 switch( equiv.toLowerCase() )
				                 {
				                    case "refresh":       seo.refresh = content; break;                    
				                 }

				            });
				          				            
							
				            var pagecheck = { mobile:mobile, seo:seo, facebook:og, twitter: tc  };

				            pagecheck.url = queueItem.url;
				            pagecheck.httpStatus = queueItem.stateData.code;				            
				            pagecheck.contentLength = queueItem.stateData.contentLength;
				            pagecheck.contentType = queueItem.stateData.contentType;

				            pagecheck.title = data.title;
				            pagecheck.favicon = data.favicon;
				            pagecheck.apple_icon = data.apple_icon;
				            if(data.ga) pagecheck.ga = data.ga;

							pagecheck._domain = dcheck._domain[0]._id,
							pagecheck._domaincheck = dcheck._id,
							pagecheck.win8 = win8;

				            pageCheckData.push(pagecheck);
				            console.log(pageCheckData.length,"PUSHED",pagecheck.url);
				            if(pageCheckData.length + 1 > limit){
				            	console.log("stopping crawl");
				            	crawler.stop();
				            	completeCrawl();//since complete event is not called when the crawler is stoped
				            	return;

				            }
						}
					  	
					  })
					 .on("fetch404",function( queueItem, response ){  
					  	var checkfile = {};
					  	checkfile.url = queueItem.url;
					  	checkfile.httpStatus = queueItem.stateData.code;			            
			            checkfile.contentLength = queueItem.stateData.contentLength;
			            checkfile.contentType = queueItem.stateData.contentType;
			            checkfile._domaincheck = dcheck._domain[0]._id,

			            domainCheckFileData.push(checkfile);			            
			            return true;

					  })
					  .on("fetcherror",function( queueItem, response ){ 
					  	var checkfile = {};
					  	checkfile.url = queueItem.url;
					  	checkfile.httpStatus = queueItem.stateData.code;			            
			            checkfile.contentLength = queueItem.stateData.contentLength;
			            checkfile.contentType = queueItem.stateData.contentType;

			             checkfile._domaincheck = dcheck._domain[0]._id,

			            domainCheckFileData.push(checkfile);

					   })
					  .on("fetchtimeout",function( queueItem, response ){
					  	  console.log("Timeout ", queueItem.url);
					   })					   
					  .on("complete",function(){ 
						completeCrawl();
					   });

					function completeCrawl(){
						
						var robots = rdata.robots;
						var humans = rdata.humans;
						var favicon = rdata.favicon;

						console.log("complete crawl");
						
						dcheck.pendingCrawl = false;						
						dcheck.totalFiles = crawler.queue.length;
						dcheck.totalPages = pageCheckData.length;
						dcheck.totalPagesChecked = pageCheckData.length;
						dcheck.brokenLinksNumber = domainCheckFileData.length;			

						dcheck.hasSitemap = urls !== null; 
						
						dcheck.hasRobotsTXT = robots !== null;
						if(dcheck.hasRobotsTXT)
							dcheck.robotsTXT = robots;

						dcheck.hasHumansTXT = humans !== null;
						if(dcheck.hasHumansTXT)
							dcheck.humansTXT = humans;

						dcheck.hasFavicon = favicon;


					    var totalSize = 0;
				        crawler.queue.forEach(function(item) {
				                if (item.fetched && item.stateData["actualDataSize"] !== null ) {
				                        totalSize = item.stateData["actualDataSize"];
				                }
				        });
				        dcheck.totalSize = totalSize; 

				        dcheck.crawlCompletedOn = new Date();

						


						PageCheck.create(pageCheckData, function (err) {
						  if (err) return console.log("Error Saving PageCheck",err);
						  for(i=1; i<arguments.length; i++){
						  	dcheck.pages.push(arguments[i]._id);
						  }

						  DomainCheckFile.create(domainCheckFileData, function (err) {
						  		if (err) return console.log("Error Saving DomainCheckFile",err);
								for(i=1; i<arguments.length; i++){
								  	dcheck.files.push(arguments[i]._id);
								}						  	
						  		dcheck.save(function(err){
							  		console.log("saved the domaincheck");
							  		console.log("now we have to remove the SQS item");
							  		sqs.deleteMessage({QueueUrl : crawlQueueUrl, ReceiptHandle: handle},function(error,item){
							  			if(err) return console.warn("Error removing the crawl item from queue");
							  			console.log("The crawl item was removed");
							  		});							  		
							  		console.log("now we have to add the Browsing SQS message");
							  		sqs.sendMessage({QueueUrl: browsingQueueUrl , MessageBody: JSON.stringify({ checkid: dcheck._id} )},
                                         function (err, result) {
                                            console.log("Browsing SQS ",err,result);
                                     });
							  		console.log("now we have to add the Validation SQS message");
							  		sqs.sendMessage({QueueUrl: validationQueueUrl , MessageBody: JSON.stringify({ checkid: dcheck._id} )},
                                         function (err, result) {
                                            console.log("Browsing SQS ",err,result);
                                            SQSReceiveMessage();
                                     });									
							  	})
						   });
						});
						
					}

	}

	SQSReceiveMessage();
