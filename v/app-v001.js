var path = require('path'),
    express = require('express'),
    http = require('http'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    GoogleStrategy = require('passport-google').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy;    

var app = express();

// Configuration
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('view options', { layout: false });

//app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(express.cookieParser('your secret here'));
app.use(express.session());

app.use(passport.initialize());
app.use(passport.session());

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.locals.pretty = true;
});

app.configure('production', function(){
    app.use(express.errorHandler());
    app.locals.pretty = true;
});

// Configure passport
var Account = require('./models/account');


passport.use(new LocalStrategy(Account.authenticate()));
passport.use(new GoogleStrategy({
    returnURL: 'http://www.launcheck.com/auth/google/return',
    realm: 'http://www.launcheck.com/'
  },
  function(identifier, profile, done) {    
    console.log(profile.emails[0].value,identifier,profile);
    Account.findOne({username: profile.emails[0].value }, function(err,account){
        if(err){
            console.log("error findOne",err);
            done(err, account); 
        }
        else if(!account)
        {
            var account = Account({ username:profile.emails[0].value,  name: {familyName: profile.name.familyName,givenName: profile.name.givenName } });            
            account.save(function(err,acc){
                done(err, acc);                
            });
        }else{
           done(err, account); 
        }  
    })
  }
));


passport.use(new FacebookStrategy({
    clientID: '253719688124908',
    clientSecret: '15a097c8e5beff9b564567d13332d92d',
    callbackURL: "http://www.launcheck.com/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    Account.findOne({username: profile.emails[0].value }, function(err,account){
        if(err){
            console.log("error findOne",err);
            done(err, account); 
        }
        else if(!account)
        {
            var account = Account({ fbid : profile.id , name: profile.name, username: profile.emails[0].value });            
            account.save(function(err,acc){
                done(err, acc);                
            });
        }else{
           done(err, account); 
        }  
    });

  }

));


passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// Connect mongoose
mongoose.connect('mongodb://admin:loremipsumpassword@paulo.mongohq.com:10047/v001');

// Setup routes
require('./routes')(app);

http.createServer(app).listen(80, '0.0.0.0', function() {
   // console.log("Express server listening on %s:%d in %s mode", '0.0.0.0', 8080, app.settings.env);
});
