var AWS = require('aws-sdk');
var mongoose = require('mongoose');
var googleapis = require('googleapis');
var sys = require('sys')
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var cuco = require('./lib/cuco');

var kue = require('kue'),
    jobs = kue.createQueue();

/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Screenshot = require('./models/screenshot');




var child;
var checkid = '';
var domaincheckid = '';

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);
AWS.config.loadFromPath('./config/config-aws.json');
var bucket = FROOTCONFIG.aws.bucket;


var s3 = new AWS.S3();
var processed = [];



    

function optimizationProcess(job,done,dcheck,pages,index){
		
	
		var item = pages[index];
		if(!item)
		{
			dcheck.pendingCuconsole = false;
			dcheck.cuconsoleCompletedOn	= new Date();
			if(dcheck.running && !dcheck.pendingBrowsing  ){
				dcheck.running = false;	
				dcheck.completed = new Date();			
			    Domain
            	.findOne({_id:dcheck._domain})                	
            	.exec(function (err, domain) {            
				  if (err) return console.warn("c",err);					 
				  domain.hasRunningCheck = false;			
				  domain.save(function(){ 
				  	console.log("Domain marked as hasRunningCheck:false");
				  });
				});
			}		
			dcheck.save(function(){						
				done();
			});
			return;
			
		}
		cuco.getGooglePageSpeedDesktop(dcheck._id,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedDesktop");
					if(err){

					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  	};

					  	item.googlePageSpeedDesktop = obj;
					  	item.save(function(){
					  		console.log(index,"Saved pagespeed Desktop");
					  	})

					  	//TODO: FALTA GUARDAR El file en s3
					}
		});
		cuco.getGooglePageSpeedMobile(dcheck._id,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedMobile");
					if(err){

					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  	};

					  	item.googlePageSpeedMobile = obj;
					  	item.save(function(){
					  		console.log(index,"Saved pagespeed Mobile");
					  	})

					  	//TODO: FALTA GUARDAR El file en s3
					}
		});


		cuco.getErrorsAndConsole(dcheck._id,item.url,function(err,res){
			console.log(index,"getErrorsAndConsole","JS Errors",res);
			if(err){
				var consoleResults = null;
				var jserrors = null;
				console.log(index,"NOT SAVING JSErrors");
			}else{
				var consoleResults = res.console;
				var jserrors = res.jserrors;	
				var obj = {
			  		count: jserrors.length,
			  		messages: jserrors			  		
			  	};
			  	var obj2 = {
			  		count: consoleResults.length,
			  		messages: consoleResults			  		
			  	};
			  	item.jsErrors = obj;
			  	item.consoleMessages = obj2;
			  	item.save(function(){
			  		console.log(index,"Saved JSErrors");
			  	});
			}				

			cuco.getHar(dcheck._id,item.url,function(err,filename){		
				console.log(index,"getHar",filename);
				if(err)
				{
					//TODO: FALTA GUARDAR EL HARRRRRRRRRR en S3
				}
				else{

				}
				cuco.getYslowWithHar(dcheck._id,item.url,filename,function(err,yslow){					
					if(err){
						console.log(index,"BROWSER: Error with yslow");
						var newindex = index + 1;
						return optimizationProcess(job,done,dcheck,pages,newindex);
					}else{

						console.log(index,"BROWSER: getYslowWithHar");
						var obj = {
					  		completedOn: new Date(),
					  		size: yslow.w,
					  		score: yslow.o,
					  		requests: yslow.r,
					  		loadtime: yslow.lt
					  	};
					  	item.yahooYSlow = obj;
					  	item.save(function(){
					  		console.log(index,"Saved yslow");
					  		var newindex = index + 1;
					  		job.progress(index,pages.length);
							return optimizationProcess(job,done,dcheck,pages,newindex);
					  	});
					}					
				});
				
			});
		});		
	
}







function slugifyUrl(text){
    text=  text.toString().toLowerCase();
    text = text.replace("http://", "");    
    text = text.replace(/\//gi, "-");
    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');
  
    text = text.replace(/\s/gi, "-");
    return text;
}


jobs.process(FROOTCONFIG.minions.cuconsole, function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	DomainCheck
	.findOne({_id:job.data.checkid})
	.populate("pages")
	.exec(function (err, dcheck) {
	  if (err) return done(err);					 
	  
	  checkid = job.data.checkid; 
	  domaincheckid = job.data._id; 
	  try{
	  	optimizationProcess(job,done,dcheck,dcheck.pages,0);
	  }catch(err){
	  	done(err);
	  }

	});
});

