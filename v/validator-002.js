var AWS = require('aws-sdk');

var mongoose = require('mongoose');
var imagemagick = require('imagemagick-native');
var fs = require('fs');
var googleapis = require('googleapis');
var sys = require('sys')
var exec = require('child_process').exec;
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');

var kue = require('kue'),
    jobs = kue.createQueue();


/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Screenshot = require('./models/screenshot');


var child;
var checkid = '';
var domaincheckid = '';

var https = require('https'),
    strategy = 'desktop';


var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);





function validate(job,done,dcheck,pages,index){


	if(pages.length-1 < index)
	{
		console.log("Validation complete");

		dcheck.pendingValidation = false;
		if(dcheck.running && !dcheck.pendingBrowsing && !dcheck.pendingCuconsole){
			dcheck.running = false;
			Domain
            	.findOne({_id:dcheck._domain})                	
            	.exec(function (err, domain) {
				  if (err) return console.warn(err);					 
				  domain.hasRunningCheck = false;
				  domain.save(function(){ 
				  	console.log("Domain marked as hasRunningCheck:false");
				  });
				});
		}
		dcheck.save(function(error,res){
			done();
		});

			
	}
	job.progress(index, pages.length);
	var item = pages[index];	
	if(!item){
		return;
	}

	(function processFile(itm) {

	var headers = { 
	    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0'
	};
		

		http.get({
	    host: 'jigsaw.w3.org', 
	    path: '/css-validator/validator?output=json&uri='+itm.url,
	    headers:headers

	    }, function(res) {
	      
	      var str = "";
	      res.on('data', function(d) {
	        str+= d.toString();
	      });
	      res.on('end',function(){	      	
	      	try{
	      		var result = JSON.parse(str);	      		
	      	}
	      	catch(e){
	      		console.warn("Error parsing cssvalidation",e,str);
	      		return;
	      	}
	      	
	      	if(result.cssvalidation){	      		
		      	var obj = {
			  		completedOn: new Date(),
			  		csslevel: result.cssvalidation.csslevel,
			  		errorCount: result.cssvalidation.result.errorcount,
			  		warningCount: result.cssvalidation.result.warningcount,
			  		valid: result.cssvalidation.validity
			  	};
			  	itm.cssValidation = obj;
			} 
			itm.save(function(err, itemoso){
				if(err){  return console.log(index,"Error saving html validation",itemoso.url,err); }
				//console.log("css",itemoso);
		

				console.log(index,"Saved cssvalidation",itemoso.url);				
			})
	      

	      });
	}).on('error', function(e) {
	  console.error(index,"error loading css validator",itm.url,e);	  
	});	

	http.get({
		    host: 'validator.w3.org', 
		    path: '/check?output=json&uri='+itm.url,
		    headers:headers
		    }, function(res) {
		      console.log(res.statusCode);
		      var str = "";
		      res.on('data', function(d) {
		        str+= d.toString();
		      });
		      res.on('end',function(){	      	
		      	
		      	try{
		      		var result = JSON.parse(str);
		      		
		      	}
		      	catch(e){		      		
		      		validate(job,done,dcheck,pages,++index); return console.log(index,"Error parsing html validation");
		      		return;
		      	}	  

		      	
		      	if(result.url){
			      	var obj = {
			      		valid: result.messages.length == 0,
				  		completedOn: new Date(),
				  		messagesCount: result.messages.length,
				  		messages: result.messages
				  	};
				  	itm.htmlValidation = obj;
				} 
				itm.save(function(err, itemoso){
					if(err){ validate(job,done,dcheck,pages,++index); return console.log(index,"Error saving html validation",err); }					
					console.log(index,"Saved htmlvalidation",itemoso.url);
					setTimeout(function(){  validate(job,done,dcheck,pages,index+1);   },1500);
				})
			    
		      });
		}).on('error', function(e) {
		  console.error(index,"error loading html validator",itm.url,e);
		  setTimeout(function(){  validate(job,done,dcheck,pages,index+1);   },1500);
		  
		});	
			
				    
		})(item);
}





function slugifyUrl(text){
    text=  text.toString().toLowerCase();
    text = text.replace("http://", "");    
    text = text.replace(/\//gi, "-");
    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');
    

    text = text.replace(/\s/gi, "-");
    return text;
}


jobs.process(FROOTCONFIG.minions.validator, function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	DomainCheck
	.findOne({_id:job.data.checkid})
	.populate("pages")
	.exec(function (err, dcheck) {
	  if (err) return console.warn(index,"Error loading the pages",err);					 					  	  
	  checkid = job.data.checkid; 
	  domaincheckid = dcheck._id; 
	  try{
	  	validate(job,done,dcheck,dcheck.pages,0);
	  }catch(err){
	  	done(err);
	  }
	});
});

