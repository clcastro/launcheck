var AWS = require('aws-sdk');
var mongoose = require('mongoose');
var googleapis = require('googleapis');
var sys = require('sys')
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var cuco = require('./lib/cuco');


/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Screenshot = require('./models/screenshot');

var bucket = 'launcheck001';


var child;
var checkid = '';
var domaincheckid = '';






mongoose.connect('mongodb://admin:loremipsumpassword@paulo.mongohq.com:10047/v001');
AWS.config.loadFromPath('./config.json');
var sqs = new AWS.SQS().client;
var s3 = new AWS.S3();
var crawlQueueUrl 		= "https://sqs.us-west-2.amazonaws.com/076753025882/crawl";
var browsingQueueUrl	= "https://sqs.us-west-2.amazonaws.com/076753025882/browsing";
var validationQueueUrl  = "https://sqs.us-west-2.amazonaws.com/076753025882/validation";
var processed = [];



     function SQSReceiveMessage(){ 
     	

        sqs.receiveMessage( { QueueUrl: browsingQueueUrl, MaxNumberOfMessages: 1,WaitTimeSeconds:20,VisibilityTimeout:3 }, function (err, job) {
                if (err) {
                        console.log(err);  
                        SQSReceiveMessage();                      
                        return;
                }

                if (!job.Messages || job.Messages.length === 0) {
                		console.log("empty messages")
                		SQSReceiveMessage();
                        return;
                }

                // Handle the message we pulled off the queue.
                var handle = job.Messages[0].ReceiptHandle,
                	body = null;
                

                try { // a JSON string message body is accepted.
                        body = JSON.parse( job.Messages[0].Body );
                } catch(e) {
                        if (e instanceof SyntaxError) {
                
                                body = JSON.parse( new Buffer(job.Messages[0].Body, 'base64').toString( 'binary' ) );
                        } else {
                                throw e;
                        }
                }
                

                if(body.checkid)
                {

                	var pageCheckData = [];
                	var domainCheckFileData = [];

                	console.log("We have a checkid, lets find the domaincheck");
                	DomainCheck
                	.findOne({_id:body.checkid})
                	.populate("pages")
                	.exec(function (err, dcheck) {
					  if (err) return console.warn(err);					 
					  
					  checkid = body.checkid; 
					  domaincheckid = dcheck._id; 
					  optimizationProcess(handle,dcheck,dcheck.pages,0);

					});
					
                }
                
        });
}

SQSReceiveMessage();

function optimizationProcess(handle,dcheck,pages,index){
		
	
		var item = pages[index];
		if(!item)
		{
			createScreenShots(handle,dcheck,pages,0);
			return console.log("End index");
		}
		cuco.getGooglePageSpeedDesktop(dcheck._id,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedDesktop");
					if(err){

					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  	};

					  	item.googlePageSpeedDesktop = obj;
					  	item.save(function(){
					  		console.log(index,"Saved pagespeed Desktop");
					  	})

					  	//TODO: FALTA GUARDAR El file en s3
					}
		});
		cuco.getGooglePageSpeedMobile(dcheck._id,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedMobile");
					if(err){

					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  	};

					  	item.googlePageSpeedMobile = obj;
					  	item.save(function(){
					  		console.log(index,"Saved pagespeed Mobile");
					  	})

					  	//TODO: FALTA GUARDAR El file en s3
					}
		});


		cuco.getErrorsAndConsole(dcheck._id,item.url,function(err,res){
			console.log(index,"getErrorsAndConsole","JS Errors",res);
			if(err){
				var consoleResults = null;
				var jserrors = null;
				console.log(index,"NOT SAVING JSErrors");
			}else{
				var consoleResults = res.console;
				var jserrors = res.jserrors;	
				var obj = {
			  		count: jserrors.length,
			  		messages: jserrors			  		
			  	};
			  	var obj2 = {
			  		count: consoleResults.length,
			  		messages: consoleResults			  		
			  	};
			  	item.jsErrors = obj;
			  	item.consoleMessages = obj2;
			  	item.save(function(){
			  		console.log(index,"Saved JSErrors");
			  	});
			}				

			cuco.getHar(dcheck._id,item.url,function(err,filename){		
				console.log(index,"getHar",filename);
				if(err)
				{
					//TODO: FALTA GUARDAR EL HARRRRRRRRRR en S3
				}
				else{

				}
				cuco.getYslowWithHar(dcheck._id,item.url,filename,function(err,yslow){					
					if(err){
						console.log(index,"BROWSER: Error with yslow");
						var newindex = index + 1;
						return optimizationProcess(handle,dcheck,pages,newindex);
					}else{

						console.log(index,"BROWSER: getYslowWithHar");
						var obj = {
					  		completedOn: new Date(),
					  		size: yslow.w,
					  		score: yslow.o,
					  		requests: yslow.r,
					  		loadtime: yslow.lt
					  	};
					  	item.yahooYSlow = obj;
					  	item.save(function(){
					  		console.log(index,"Saved yslow");
					  		var newindex = index + 1;
							return optimizationProcess(handle,dcheck,pages,newindex);
					  	});
					}					
				});
				
			});
		});		
	
}


function createScreenShots(handle,dcheck,pages,index){
	if(pages.length-1 < index)
		{
			console.log("files saved");

			dcheck.pendingBrowsing = false;					
			if(dcheck.running ){
				dcheck.running = false;
				console.log("a");
			    Domain
            	.findOne({_id:dcheck._domain})                	
            	.exec(function (err, domain) {
            	  console.log("b");
				  if (err) return console.warn("c",err);					 
				  domain.hasRunningCheck = false;
				  console.log("d");
				  domain.save(function(){ 
				  	console.log("Domain marked as hasRunningCheck:false");
				  });
				});
			}
			console.log("e");

			dcheck.save(function(){			
				console.log("f");
				sqs.deleteMessage({QueueUrl : browsingQueueUrl, ReceiptHandle: handle},function(error,item){
					console.log("g");
		  			if(error) return console.warn(index,"Error removing the BROWSER SQS item from queue");
		  			processed.push(dcheck._id);
		  			console.log(index,"The BROWSER SQS item was removed");
		  			console.log("h");
		  			SQSReceiveMessage(); 		
		  		});	
			});
			console.log("i");
			
	}
	//try{
		

		var item = pages[index];
		if(!item){
			return setTimeout(function(){ SQSReceiveMessage(); }, 1000);
		}
		console.log(item.url);


		//We do all the process that involves PHANTOMJS secuencially becasue they have a heavy CPU use
		function processScreenshot(err,indx,img,thumb,imgData,thumbData,filesize,filename){
			console.log("Screenshot ",indx," called");
			if(err){
				//Here we would have to save that the screenshot was generated with errors
				//By now we just call the next screenshot
				//console.log("getting screenshot ",indx+1,err);
				//cuco.getScreenshot(dcheck._id,item.url,indx+1,processScreenshot);

			}else{
				//console.log("getting screenshot ",indx+1);
				//cuco.getScreenshot(dcheck._id,item.url,indx+1,processScreenshot);
				if(indx == cuco.screenshotSizes.length-1 ){
					console.log("all screenshots done");
					console.log("GARBAGE CLEAN");
					createScreenShots(handle,dcheck,pages,++index);
				}
				if(indx <= cuco.screenshotSizes.length-1){								
					
					var filekey = filename+".jpg";
					var thumbkey = filename+"thumb.jpg";
					s3.client.putObject({
					    Bucket: bucket,
					    Key: filekey,
					    Body: img,
					    ContentType: 'image/jpeg'
					  },function (err) {
					    if(err){ console.error(index,"Error saving the image",err); }
						console.log("Uploaded Image ",index,indx)
						 s3.client.putObject({
						    Bucket: bucket,
						    Key: thumbkey,
						    Body: thumb,
						    ContentType: 'image/jpeg'
						  },function (err) {
								if(err){
									console.error(index,"Error saving the thumb",err);
								}else{
									console.log("saved thumb",index,indx);
									Screenshot.create(
									{
										_domaincheck : item._domaincheck,
									    _pagecheck:  item._id,
									    created: new Date(), 
									    bucket: bucket,
									    key: filekey,
									    keyThumb: thumbkey,
									    width: cuco.screenshotSizes[indx][0],
									    height: cuco.screenshotSizes[indx][1],
									    realWidth: imgData.width,
									    realHeight: imgData.height,    
									    thumbnailRealWidth:thumbData.width,
									    thumbnailRealHeight:thumbData.height,
									    fileSize: filesize												    
									}, function (err) {
										if(err)	return console.error(index,"Error saving the screenshot");
										console.log(index,indx,"saved screenshot",item.url,item._id);
									});

								}
						  });
						
					  });				

				}else{
					console.log("all screenshots done");
					console.log("GARBAGE CLEAN");
					createScreenShots(handle,dcheck,pages,++index);
				}
			}
		}			
					


		for(k=0; k < cuco.screenshotSizes.length;k++){								
			console.log("getting screenshot ",k);
			cuco.getScreenshot(dcheck._id,item.url,k,processScreenshot);			
		}	

	/***************************************************************	
		
		cuco.getGooglePageSpeedDesktop(dcheck._id,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedDesktop",pagespeed.score);
					if(err){

					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  	};

					  	item.googlePageSpeedDesktop = obj;
					  	item.save(function(){
					  		console.log(index,"Saved pagespeed Desktop");
					  	})

					  	//TODO: FALTA GUARDAR El file en s3
					}
		});
		cuco.getGooglePageSpeedMobile(dcheck._id,item.url,function(err,pagespeed){
					console.log("getGooglePageSpeedMobile",pagespeed.score);
					if(err){

					}else{					
				      	var obj = {
					  		completedOn: new Date(),
					  		size: pagespeed.pageStats.totalRequestBytes,
					  		score: pagespeed.score,
					  		requests: pagespeed.pageStats.numberResources,	  		
					  	};

					  	item.googlePageSpeedMobile = obj;
					  	item.save(function(){
					  		console.log(index,"Saved pagespeed Mobile");
					  	})

					  	//TODO: FALTA GUARDAR El file en s3
					}
		});

	************************************************************/
	/*}catch(e){
		console.log("UNKNOWN ERROR, the show must continue",e);
		createScreenShots(handle,dcheck,pages,++index);
	}*/
	/*

	console.log(index,"Starting yslow");
  	var childyslow = exec(
	  "phantomjs yslow.js --info basic "+item.url,						  
	  function (error, stdout, stderr) {
	  	console.log("yslow results");
	  	if(error) return console.error(index,"Error getting yslow");
	  	try{
	  		var yslow = JSON.parse(stdout.toString() );
	  	}
	  	catch(e){
	  		console.warn(index,"Error parsing yslow");
	      	return;
	  	}
	  	var obj = {
	  		completedOn: new Date(),
	  		size: yslow.w,
	  		score: yslow.o,
	  		requests: yslow.r,
	  		loadtime: yslow.lt
	  	};
	  	item.yahooYSlow = obj;
	  	item.save(function(){
	  		console.log(index,"Saved yslow");
	  	});
	  });

	  console.log(index,"pagespeedonline");
	

	https.get({
	    host: 'www.googleapis.com', 
	    path: '/pagespeedonline/v1/runPagespeed?url=' + encodeURIComponent(item.url) + 
	          '&key=AIzaSyBNskxT8SAXwaBXslwLZFpstzTB8mNOZB0&strategy='+'desktop'
	    }, function(res) {
	      
	      var str = "";
	      res.on('data', function(d) {
	        str+= d.toString();
	      });
	      res.on('end',function(){
	      	try{
	      		var pagespeed = JSON.parse(str);
	      	}
	      	catch(e){
	      		console.warn(index,"Error parsing pagespeed");
	      		return;
	      	}
	      	if(pagespeed.pageStats){
		      	var obj = {
			  		completedOn: new Date(),
			  		size: pagespeed.pageStats.totalRequestBytes,
			  		score: pagespeed.score,
			  		requests: pagespeed.pageStats.numberResources,	  		
			  	};

			  	item.googlePageSpeed = obj;
			  	item.save(function(){
			  		console.log(index,"Saved pagespeedonline");
			  	})
		    }

	      });
	}).on('error', function(e) {
	  console.error(e);
	});






	var sizes = ["320x480","640x480","1024x768"];	//"768x1024",
	//var sizes = ["1024x768"];	
	var cmd =  "phantomjs /var/www/launcheckv1/phantom-capture.js "+item.url+" "+slugifyUrl(item.url)+"  "+sizes.join(",");
	console.log(cmd);
  	var child = exec(
	  cmd,						  
	  function (error, stdout, stderr) {
	    console.log(arguments);
	    if(error){
	    	//we go for the nex screenshot nevertheless
	    	createScreenShots(handle,dcheck,pages,++index);
	    	return console.error(index,"Error creating asset");	
	    } 
	    
	    createScreenShots(handle,dcheck,pages,++index);
	    //GET ALL THE SCREENSHOTS AND CREATE THE THUMBNAILS								

	    for(i = 0; i<sizes.length;i++){
	    	var size = sizes[i];
	    	var slug = slugifyUrl(item.url);
	    	var filename = slug + size;

			(function processFile(file,widthXheight,itm) {

				var srcData  = fs.readFileSync('./temp/'+file+'.jpg');
				var fileStats =  fs.statSync('./temp/'+file+'.jpg')

				var rezisedBuffer = imagemagick.convert({
				    srcData: srcData, // provide a Buffer instance
				    width: 128,
				    height: 128,
				    resizeStyle: "aspectfit",
				    quality: 80,
				    format: 'JPEG'
				});	

				  var screenshotData =  imagemagick.identify({srcData: srcData });
				  var screenshotThumbData =  imagemagick.identify({srcData: rezisedBuffer });
				  var fileSize = fileStats["size"];
				  var key = checkid+"-"+file+".jpg";

				
				  s3.client.putObject({
				    Bucket: bucket,
				    Key: key,
				    Body: srcData,
				    ContentType: 'image/jpeg'
				  },function (err) {
				    if(err)	console.error("Error saving the image");
				    console.log("saved image");
					if(fs.existsSync('./temp/'+file+'.jpg') )
				   		fs.unlinkSync('./temp/'+file+'.jpg');

				   	var key = checkid+"-"+file+'thumb.jpg';
				   	
					
					  s3.client.putObject({
					    Bucket: bucket,
					    Key: key,
					    Body: rezisedBuffer,
					    ContentType: 'image/jpeg'
					  },function (err) {
							if(err)	console.error(index,"Error saving the thumb");
							console.log(index,"saved thumb");
							var part = widthXheight.split("x");

							console.log(itm._domaincheck)
							Screenshot.create(
							{
								_domaincheck : itm._domaincheck,
							    _pagecheck:  itm._id,
							    created: new Date(), 
							    bucket: bucket,
							    key: file+'.jpg',
							    keyThumb: file + 'thumb.jpg',
							    width: parseInt(part[0]),
							    height: parseInt(part[1]),
							    realWidth: screenshotData.width,
							    realHeight: screenshotData.height,    
							    thumbnailRealWidth:screenshotThumbData.width,
							    thumbnailRealHeight:screenshotThumbData.height,
							    fileSize: fileSize												    
							}, function (err) {
								if(err)	return console.error(index,"Error saving the screenshot");
								console.log(index,"saved screenshot",item.url,item._id);

							});
					  });
				  });
				    
			})(filename,size,item);
		}
	  }
	); 

*/

}




function slugifyUrl(text){
    text=  text.toString().toLowerCase();
    text = text.replace("http://", "");    
    text = text.replace(/\//gi, "-");
    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');
  
    text = text.replace(/\s/gi, "-");
    return text;
}
