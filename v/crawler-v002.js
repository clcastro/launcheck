var mongoose = require('mongoose');
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var check4 = require('./lib/check4');
var kue = require('kue'),
    jobs = kue.createQueue();

var ocrawler = require('./lib/ocrawler');

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);

var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var parseString = require('xml2js').parseString;
var limit = 8;





	function processDomainCheck(job,done,dcheck,rdata){
				
					var urls = rdata.sitemap;
					

					var pageCheckData = [];
                	var domainCheckFileData = [];
                	var sitemapFound = false;
					 var checkurl =  dcheck._domain[0].hostname + ( (dcheck._domain[0].port == 80 ||  !dcheck._domain[0].port )?"":(":"+dcheck._domain[0].port)  ) ;
					  console.log("Checking: ",checkurl)

					  //var crawler = Crawler.crawl(checkurl);					  
					  var crawler = new Crawler(checkurl);
					  if(urls && urls.length > 0)
					  {
					  	sitemapFound = true;
					  	x = 0;
					  	urls.forEach(function(item) {
					  	 	if(x > limit) return;	
					  	   console.log(x,limit,"adding url",item.loc[0]);
			               crawler.queueURL(item.loc[0]);
			               x++;
			              			               
				        });
					  }
					  
					  crawler.addFetchCondition( function(parsedURL) {
			                  return !parsedURL.path.match(/\.(pdf|png|otf|jpg|gif|jpeg|svg|eot|woff|ttf|htc|js)$/i);
			          });
						
					  crawler			  					  
					  .on("fetchcomplete",function(queueItem, responseBuffer , response ){ 
					  	console.log("fetchcomplete");
					  	var url = queueItem.url;
						console.log("fetched:",url);

						if(queueItem.stateData  && queueItem.stateData.contentType &&  queueItem.stateData.contentType.match(/text\/html/gi) ){
							if(pageCheckData.length + 1 > limit){
				            	console.log("stopping crawl at the begining, total:",pageCheckData.length," maximum: ",limit);				            	
				            	return;
				            }									

						    var html = responseBuffer.toString(); 
							var data = {};
							try{
						        var $html = $(html);
					    	}catch(e){					    		
					    		console.log("Error parsing HTML",url);
					    		return;
					    	}

					    	data.html5 = {is: html.indexOf("<!DOCTYPE html>") > -1 };
				            data.title = $html.find("head title").text();                                    //sacamos el titulo
				            data.favicon = $html.find('link[rel~="icon"]').attr("href");                   //sacamos el favicon
				            $aicons = $html.find('link[rel="apple-touch-icon"],link[rel="apple-touch-icon-precomposed"]');
				            data.apple_icon = $aicons.length > 0 ? $($aicons[0]).attr("href"):'';				            				            
				            var gaRegExp = /_gaq.push\(\['_setAccount'\s*,\s*'(UA-\w*-\w)'\]\s*\)\s*;/gi; 
				            var match  = html.match(gaRegExp);
				            var ga = match && match[1];
				            if( ga ) data.ga = ga; 
				            
				      
				            var og = {};
				            var tc = {};
				            var info = {};
				            var seo = {};
				            var mobile = {};
				            var win8 ={};


				            $html.find("head meta").each(function(){
				                 $meta = $(this);
				                 var content = $meta.attr("content");    
				                 var property = $meta.attr("property")+"";
				                 var name = $meta.attr("name")+"";
				                 var equiv = $meta.attr("http-equiv")+"";
				                 console.log("property",property,name,content);
				                 switch( property.toLowerCase() )
				                 { 
				                
				                    case "og:site_name": og.site_name = content; break;  
				                    case "og:title":     og.title = content; break;
				                    case "og:description":     og.description = content; break;
				                    case "og:image":     og.image = content; break;
				                    case "og:type":      og.type = content; break;
				                    case "og:url":       og.url = content; break;
				                    case "fb:admins":    og.fb_admins = content; break;

				                    case "twitter:card":         tc.card = content; break;
				                    case "twitter:url":          tc.url = content; break;
				                    case "twitter:title":        tc.title = content; break;
				                    case "twitter:description":  tc.description = content; break;
				                    case "twitter:image":        tc.image = content; break;
				                    case "twitter:site":         tc.site = content; break;
				                    case "twitter:creator":      tc.creator = content; break;

				                 }

				                 switch( name.toLowerCase() )
				                 {
				                    case "author":       seo.author = content; break;
				                    case "description":  seo.description = content; break;
				                    case "generator":    seo.generator = content; break;
				                    case "keywords":     seo.keywords = content; break;
				                    case "robots":     seo.robots = content; break;
				                    case "generator":     seo.generator = content; break;
				                    case "viewport":             mobile.viewport = content; break;   
				                    case "application-name": win8.applicationName = content; break;
				                    case "msapplication-TileColor": win8.tileColor = content; break;
				                    
				                 }

				                 switch( equiv.toLowerCase() )
				                 {
				                    case "refresh":       seo.refresh = content; break;                    
				                 }

				            });
				          				            
							
				            var pagecheck = { mobile:mobile, seo:seo, facebook:og, twitter: tc  };

				            console.log(pagecheck);

				            pagecheck.url = queueItem.url;
				            pagecheck.httpStatus = queueItem.stateData.code;				            
				            pagecheck.contentLength = queueItem.stateData.contentLength;
				            pagecheck.contentType = queueItem.stateData.contentType;

				            pagecheck.title = data.title;
				            pagecheck.favicon = data.favicon;
				            pagecheck.apple_icon = data.apple_icon;
				            if(data.ga) pagecheck.ga = data.ga;

							pagecheck._domain = dcheck._domain[0]._id,
							pagecheck._domaincheck = dcheck._id,
							pagecheck.win8 = win8;

				            pageCheckData.push(pagecheck);
				            console.log(pageCheckData.length,"PUSHED",pagecheck.url);
				            if(pageCheckData.length + 1 > limit){
				            	console.log("stopping crawl");
				            	crawler.stop();
				            	completeCrawl();//since complete event is not called when the crawler is stoped
				            	return;

				            }
						}
					  	
					  })
					 .on("fetch404",function( queueItem, response ){  
					 	console.log("fetch404");
					  	var checkfile = {};
					  	checkfile.url = queueItem.url;
					  	checkfile.httpStatus = queueItem.stateData.code;			            
			            checkfile.contentLength = queueItem.stateData.contentLength;
			            checkfile.contentType = queueItem.stateData.contentType;
			            checkfile._domaincheck = dcheck._domain[0]._id,

			            domainCheckFileData.push(checkfile);			            
			            return true;

					  })
					  .on("fetcherror",function( queueItem, response ){ 
					  	console.log("fetcherror");
					  	var checkfile = {};
					  	checkfile.url = queueItem.url;
					  	checkfile.httpStatus = queueItem.stateData.code;			            
			            checkfile.contentLength = queueItem.stateData.contentLength;
			            checkfile.contentType = queueItem.stateData.contentType;

			             checkfile._domaincheck = dcheck._domain[0]._id,

			            domainCheckFileData.push(checkfile);

					   })
					  .on("fetchtimeout",function( queueItem, response ){
					  	  console.log("Timeout ", queueItem.url);
					   })					   
					  .on("complete",function(){ 
					  	console.log("COMPLETE EVENT");
						
					   })
					  .on("discoverycomplete",function(){ 
					  	console.log("discoverycomplete");
						
					   })
					  .on("fetchclienterror",function(queueItem,errorData){ 
					  	console.log("fetchclienterror ",queueItem);
					  	console.error(errorData.stack);
						
					   })
					  .on("queueerror",function(){ 
					  	console.log("queueerror  EVENT");
						
					   })
					  .on("fetchdataerror",function(){ 
					  	console.log("queueerror  EVENT");
						
					   })
					  .on("crawlstart ",function(){ 
					  	console.log("crawlstart ");						
					   });
					  
					  crawler.start();



					function completeCrawl(){
						
						var robots = rdata.robots;
						var humans = rdata.humans;
						var favicon = rdata.favicon;

						console.log("complete crawl");
						
						dcheck.pendingCrawl = false;						
						dcheck.totalFiles = crawler.queue.length;
						dcheck.totalPages = pageCheckData.length;
						dcheck.totalPagesChecked = pageCheckData.length;
						dcheck.brokenLinksNumber = domainCheckFileData.length;			

						dcheck.hasSitemap = urls !== null; 
						
						dcheck.hasRobotsTXT = robots !== null;
						if(dcheck.hasRobotsTXT)
							dcheck.robotsTXT = robots;

						dcheck.hasHumansTXT = humans !== null;
						if(dcheck.hasHumansTXT)
							dcheck.humansTXT = humans;

						dcheck.hasFavicon = favicon;


					    var totalSize = 0;
				        crawler.queue.forEach(function(item) {
				                if (item.fetched && item.stateData["actualDataSize"] !== null ) {
				                        totalSize = item.stateData["actualDataSize"];
				                }
				        });
				        dcheck.totalSize = totalSize; 

				        dcheck.crawlCompletedOn = new Date();

						


						PageCheck.create(pageCheckData, function (err) {
						  if (err) return console.log("Error Saving PageCheck",err);
						  for(i=1; i<arguments.length; i++){
						  	dcheck.pages.push(arguments[i]._id);
						  }

						  DomainCheckFile.create(domainCheckFileData, function (err) {
						  		if (err) return console.log("Error Saving DomainCheckFile",err);
								for(i=1; i<arguments.length; i++){
								  	dcheck.files.push(arguments[i]._id);
								}						  	
						  		dcheck.save(function(err){
							  		console.log("saved the domaincheck");

							  		jobs.create(FROOTCONFIG.minions.cuconsole,{checkid:dcheck._id,title:"cuconsole "+checkurl}).priority('high').attempts(2).save();
									jobs.create(FROOTCONFIG.minions.browser,{checkid:dcheck._id,title:"browsing "+checkurl}).priority('high').attempts(2).save();
									jobs.create(FROOTCONFIG.minions.validator,{checkid:dcheck._id,title:"validating "+checkurl}).priority('high').attempts(2).save();

									delete pageCheckData;
									delete domainCheckFileData;
									delete rdata;		
									delete crawler; //DANGER, DANGER
									job.progress("100", "100");							
									done();
									job.log("Job marked as completed");
									console.log("DONE!");

							  	})
						   });
						});
						
					}

	}




jobs.process(FROOTCONFIG.minions.crawler, function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	DomainCheck
	.findOne({_id:job.data.checkid})
	.populate("_domain")
	.exec(function (err, dcheck) {
		if (err) return console.warn("error getting domain ",err);		
		var urlobj = {hostname:dcheck._domain[0].hostname,port:dcheck._domain[0].port,path:dcheck._domain[0].path};  

		check4.robots_txt( urlobj, function(err,robots){			
			check4.sitemap( urlobj, function(err,smap){  						
				check4.humans_txt( urlobj, function(err,humans){  
					check4.favicon( urlobj, function(err,isfavicon){  
						var rdata = {sitemap:smap,robots:robots,humans:humans,favicon:isfavicon};					
						job.log("starting to crawl");


						processDomainCheck(job,done,dcheck,rdata);
						
					});	
				});	
			});
		});
	});
});

