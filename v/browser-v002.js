var AWS = require('aws-sdk');
var mongoose = require('mongoose');
var googleapis = require('googleapis');
var sys = require('sys')
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var cuco = require('./lib/cuco');

var kue = require('kue'),
    jobs = kue.createQueue();

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);
AWS.config.loadFromPath('./config/config-aws.json');

/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Screenshot = require('./models/screenshot');

var bucket = FROOTCONFIG.aws.bucket;

var child;
var checkid = '';
var domaincheckid = '';

var s3 = new AWS.S3();
var processed = [];


function createScreenShots(job,done,dcheck,pages,index){
	try {
	if(pages.length-1 < index)
		{
			console.log("files saved");

			dcheck.pendingBrowsing = false;	
			dcheck.cuconsoleCompletedOn = new Date();							
			if(dcheck.running ){
				dcheck.completed = new Date();
				dcheck.running = false;				
			    Domain
            	.findOne({_id:dcheck._domain})                	
            	.exec(function (err, domain) {            	
				  if (err) return console.warn("c",err);					 
				  else{
					  domain.hasRunningCheck = false;
					
					  domain.save(function(){ 
					  	console.log("Domain marked as hasRunningCheck:false");
					  });
				  }
				});
			}			
			dcheck.save(function(){					
				done();
			});						
	}
	//try{
		

		var item = pages[index];
		job.progress(index+pages.length, pages.length*2);
		if(!item){
			return;
		}
		console.log(item.url);

		//We do all the process that involves PHANTOMJS secuencially becasue they have a heavy CPU use
		function processScreenshot(err,indx,img,thumb,imgData,thumbData,filesize,filename){
			console.log("Screenshot ",indx," called");
			if(err){
				//Here we would have to save that the screenshot was generated with errors
				//By now we just call the next screenshot
				//console.log("getting screenshot ",indx+1,err);
				//cuco.getScreenshot(dcheck._id,item.url,indx+1,processScreenshot);

			}else{
				//console.log("getting screenshot ",indx+1);
				//cuco.getScreenshot(dcheck._id,item.url,indx+1,processScreenshot);
				if(indx == cuco.screenshotSizes.length-1 ){
					console.log("all screenshots done");
					console.log("GARBAGE CLEAN");
					createScreenShots(job,done,dcheck,pages,++index);
				}
				if(indx <= cuco.screenshotSizes.length-1){								
					
					var filekey = filename+".jpg";
					var thumbkey = filename+"thumb.jpg";
					s3.client.putObject({
					    Bucket: bucket,
					    Key: filekey,
					    Body: img,
					    ContentType: 'image/jpeg'
					  },function (err) {
					    if(err){ console.error(index,"Error saving the image",err); }
						console.log("Uploaded Image ",index,indx)
						 s3.client.putObject({
						    Bucket: bucket,
						    Key: thumbkey,
						    Body: thumb,
						    ContentType: 'image/jpeg'
						  },function (err) {
								if(err){
									console.error(index,"Error saving the thumb",err);
								}else{
									console.log("saved thumb",index,indx);
									Screenshot.create(
									{
										_domaincheck : item._domaincheck,
									    _pagecheck:  item._id,
									    created: new Date(), 
									    bucket: bucket,
									    key: filekey,
									    keyThumb: thumbkey,
									    width: cuco.screenshotSizes[indx][0],
									    height: cuco.screenshotSizes[indx][1],
									    realWidth: imgData.width,
									    realHeight: imgData.height,    
									    thumbnailRealWidth:thumbData.width,
									    thumbnailRealHeight:thumbData.height,
									    fileSize: filesize												    
									}, function (err) {
										if(err)	return console.error(index,"Error saving the screenshot");
										console.log(index,indx,"saved screenshot",item.url,item._id);
									});

								}
						  });
						
					  });				

				}else{
					console.log("all screenshots done");
					console.log("GARBAGE CLEAN");
					createScreenShots(job,done,dcheck,pages,++index);
				}
			}
		}			
					
		for(k=0; k < cuco.screenshotSizes.length;k++){								
			console.log("getting screenshot ",k);
			cuco.getScreenshot(dcheck._id,item.url,k,processScreenshot);			
		}	
	}catch(err){
		console.log("MUERTO CON ERROR");
		console.error(err);
		console.error(err.stack);
		done(err);
	}
}




function slugifyUrl(text){
    text=  text.toString().toLowerCase();
    text = text.replace("http://", "");    
    text = text.replace(/\//gi, "-");
    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');
  
    text = text.replace(/\s/gi, "-");
    return text;
}


jobs.process(FROOTCONFIG.minions.browser, function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	DomainCheck
	.findOne({_id:job.data.checkid})
	.populate("pages")
	.exec(function (err, dcheck) {
	  if (err) return done(err);					 
	  
	  checkid = job.data.checkid; 
	  domaincheckid = job.data._id; 
	  try{
	  	createScreenShots(job,done,dcheck,dcheck.pages,0);
	  }catch(err){
	  	done(err);
	  }

	});
});

