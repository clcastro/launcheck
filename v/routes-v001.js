var passport = require('passport'),
    Account = require('./models/account'),
    Domain = require('./models/domain'),
    DomainCheck = require('./models/domaincheck'),
    Screenshot = require('./models/screenshot'),
    //http = require('http'),
    http = require('follow-redirects').http;
    $ = require('jquery'),
    AWS = require('aws-sdk'),
    url = require('url');

AWS.config.loadFromPath('./config.json');
var sqs = new AWS.SQS().client;
var queueUrl = "https://sqs.us-west-2.amazonaws.com/076753025882/crawl";

module.exports = function (app) {

    app.get('/auth/google', passport.authenticate('google'))
    app.get('/auth/google/return', 
    passport.authenticate('google', { successRedirect: '/',
                                    failureRedirect: '/login' }));

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }) );
    app.get('/auth/facebook/callback', 
    passport.authenticate('facebook', { successRedirect: '/',
                                    failureRedirect: '/login' }));

    app.get('/', function (req, res) {


        if(req.user){
            DomainCheck
                .find({_uid:req.user._id})
                .populate("_domain")
                .populate("checks")
                .limit(20)
                .sort('-created')
                .exec(function(err,checks){                                
                     res.render('index', { checks:checks, user : req.user });
                });
        }
        else{
         DomainCheck
                .find({_uid:{$exists:false}})
                .populate("_domain")
                .populate("checks")
                .limit(10)
                .sort('-created')
                .exec(function(err,checks){                                
                     res.render('index', { checks:checks, user : req.user });
                });
        }


    });

    app.get('/register', function(req, res) {
        res.render('register', { });
    });

    app.post('/register', function(req, res) {
        Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
            if (err) {
                return res.render('register', { account : account });
            }

            res.redirect('/');
        });
    });

    app.post('/check', function(req, res) {


        var baseurl = req.body.url;
        if( baseurl.indexOf("http://") < 0 ) {
            baseurl = "http://" + baseurl ;
        }        
        var checkurl = url.parse(baseurl);
        console.log(checkurl);
        var nreq = http.request(checkurl, function(httpres) {
                if(httpres.statusCode == 200){
                    /**  The url is ok so we continue **/                    
                    Domain.findOne({protocol:checkurl.protocol,hostname:checkurl.hostname,path:checkurl.path}, function (err, dmn) {
                        if (err) console.warn(err); 
                        if(!dmn){
                            //The domain does not exists for this account
                            //Create the domain with the default check config and add it to the queue
                            console.log("let's create the domain and the domaincheck");
                            var domain = new Domain({ created: new Date(), hasRunningCheck:false, protocol: checkurl.protocol, hostname: checkurl.hostname, port: checkurl.port || 80, path: checkurl.path,  testsCount: 0 });
                            domain.save(function (err) {
                                      if (err) return console.warn(err);
                                      // saved!
                                      console.log("Domain:",domain._id);
                                      
                                      var dcheck = new DomainCheck({  });
                                      dcheck._domain = domain;
                                      dcheck.created = new Date();
                                      dcheck.running = true;


                                      dcheck.save(function(error,dc){
                                        domain.checks.push( dcheck );
                                        domain.hasRunningCheck = true;
                                        domain.save(function (err) {
                                           if (err) return console.warn(err);         
                                            console.log("suceess");
                                            // En este momento debieramos encolar el crawling 
                                            console.log("We queue the crawler");
                                            sqs.sendMessage({QueueUrl: queueUrl , MessageBody: JSON.stringify({ checkid: dcheck._id} )},
                                             function (err, result) {
                                                console.log("SQS",err,result);
                                                res.redirect('/check/'+dc._id);
                                            });
                                        });

                                      });                                      
                            });


                        }else{
                            console.log("Domain is present");
                            if(dmn.hasRunningCheck){
                                console.log("Domain has running check");
                                res.redirect('/'); //We tell that the domain has a check already running in this account
                            }
                            else{
                                //Create the check with the default check config and add it to the queue
                                  console.log("let's create the domaincheck");
                                  var dcheck = new DomainCheck({  });
                                  dcheck._domain = dmn._id;
                                  dcheck.created = new Date();
                                  dcheck.running = true;
                                  if(req.user){
                                        dcheck._uid = req.user._id;
                                        console.log("Logged user:",req.user._id);
                                  }


                                  dcheck.save(function(error,dc){
                                    dmn.checks.push( dcheck );
                                    dmn.hasRunningCheck = true;
                                    dmn.save(function (err) {
                                       if (err) return console.warn(err);         
                                        console.log("suceess");
                                        // En este momento debieramos encolar el crawling 
                                        console.log("We queue the crawler");

                                        sqs.sendMessage({QueueUrl: queueUrl , MessageBody: JSON.stringify({ checkid: dcheck._id} )},
                                         function (err, result) {
                                            console.log("SQS",err,result);

                                        });

                                        res.redirect('/check/'+dc._id);

                                    });

                                  });                                      

                            }
                        }

                    });                    
                    /**********************************/
                }
                else{
                    console.log("Error with the domain");
                    res.redirect('/'); //We tell that we had an error with the url
                }
                httpres.on('end',function(){  });
        });
        nreq.on('error', function(e) {
          console.log('problem with request: ' + e.message,e);
        });
        nreq.end();
        
    });

    app.get('/check', function(req, res) {
        res.render('single',  {layout:'layoutsingle.jade' });
    });

    app.get('/check/:id', function(req, res) {
        var checkid = req.params.id;
        DomainCheck
            .findOne({_id:checkid})
            .populate("_domain")
            .populate("pages")
           .populate("files")
            //.populate("pagechecks pages")
            .exec(function (err, dcheck) {
              if (err) return console.warn(err);                                   
              Screenshot
                .find({_domaincheck:checkid})
                .exec(function(err,shots){                    
                    if(dcheck.pendingCrawl)
                    	res.render('single-crawling',  { check:dcheck,screenshots:shots ,layout:'layoutsingle.jade'});
                	else
                		res.render('single',  { check:dcheck,screenshots:shots ,layout:'layoutsingle.jade'});
                });

            });
    });



    app.get('/login', function(req, res) {
        res.render('login', { user : req.user });
    });

    app.post('/login', passport.authenticate('local'), function(req, res) {
        res.redirect('/');
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
};
