var page = require('webpage').create(),
    system = require('system');

if (system.args.length === 1) {
    console.log('Usage: netsniff.js <some URL>');
    phantom.exit(1);
} else {
	var out = {console:[],jserrors:[]};
    page.address = system.args[1];
    
    page.onLoadStarted = function () {
        page.startTime = new Date();
    };

    page.onConsoleMessage = function(msg, lineNum, sourceId) {
      out.console.push({line:lineNum,msg:msg,source:sourceId});	  
	};


	page.onError = function(msg, trace) {
	  var error = {msg:msg,trace:[]}
  	  var msgStack = [];
	  if (trace && trace.length) {
	    msgStack.push('TRACE:');
	    trace.forEach(function(t) {	     
	      msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
	    });
	  }
	  error.trace = msgStack;
  	  out.jserrors.push(error);

	};


    page.open(page.address, function (status) {
        
        if (status !== 'success') {
            console.log('FAIL to load the address');
            phantom.exit(1);
        } else {
        	setTimeout(function(){ 
	            page.endTime = new Date();
	            page.title = page.evaluate(function () {
	                return document.title;
	            });            
	            console.log(JSON.stringify(out, undefined, 4));
	            phantom.exit();
        	},
            1000);
        }
    });

}