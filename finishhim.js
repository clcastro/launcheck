var AWS = require('aws-sdk');
var nodemailer = require('nodemailer');
var mongoose = require('mongoose');
var imagemagick = require('imagemagick-native');
var fs = require('fs');
var googleapis = require('googleapis');
var sys = require('sys')
var exec = require('child_process').exec;
var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');

var kue = require('kue'),
    jobs = kue.createQueue();


/*MODELS*/
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Task = require('./models/task');
var Account = require('./models/account');

var child;
var checkid = '';
var domaincheckid = '';

var https = require('https'),
    strategy = 'desktop';


var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);


jobs.process(FROOTCONFIG.minions.finishhim,  function(job, done){
	

	DomainCheck	
	.findOne({_id:job.data.checkid })	
	.exec(function (err, check) {
	 
	  console.log("finishin ",check._id)
	  if(!check) 
	  {
	  	console.log("PAGE NOT FOUND");
	  	done(err);
	  }
	  if (err){
	   console.warn("Error loading the page",err);					 					  	  
	   done(err);
	  }	  
	  try{
	  	check.running = false;
	  	check.completed = new Date();
	  	check.sucessfull = true;
	  	Task
		.find({checkid:job.data.checkid })
		.select('status')
		.exec(function (err, tasks) {
			if(tasks){
				console.log("found ",tasks.length," tasks");

				var logs = [];
				for(i=0;i<tasks.length;i++){
					var task = tasks[i] 
					if(task.status == 'failed')
					{
						logs = [{pageid: task.pageid,name : task.name,errors:task.log}]
					}
					task.remove();
				}
				check.logs = logs;
			}
			check._domain.hasRunningCheck = false;
			console.log('findByIdAndUpdate',check._domain)			
			Domain.findByIdAndUpdate(check._domain, { $set: { hasRunningCheck: false }},function(){
				console.log('findByIdAndUpdate-response',arguments)
				check.save(function(){  
				console.log("check saved");
				done();
			});

			})

			
		});

	  	if(check._uid)
	  	{
	  		Account
			.findOne({_id:check._uid })
			.exec(function (err, user) {
				

				// Create a Direct transport object
				var transport = nodemailer.createTransport("SMTP", {
				        service: 'Mandrill', 
				        auth: {
				            user: "yayolius@gmail.com",
				            pass: "BOrpSZ7b9KoLe_tExQbRjQ"
				        }
				    });

				// Message object
				var message = {
				    // sender info
				    from: 'Sender Name <sender@launcheck.com>',
				    // Comma separated list of recipients
				    to: user.username,
				    // Subject of the message
				    subject: 'Your check is ready ✔ 2', //
				    // plaintext body
				    text: 'done!',
				    // HTML body
				    html:'<p><b>Hello</b> your check from launcheck is completed</p>'+
				         '<p>Go here <a href="http://www.launcheck.com/check/'+check._id+'">http://www.launcheck.com/check/'+check._id+'</a> to see the results.</p>'+
				         '<p>The FrootBox Team</p>',
				};

				console.log('Sending Mail');

				transport.sendMail(message, function(error, response){
				    if(error){
				        console.log('Error occured');
				        console.log(error.message);
				        return;
				    }else{
				        console.log(response);
				        console.log('Message sent successfully!');
				    }

				});


			});
	  	}
	  	
	  }catch(err){
	  	console.log("ERRRORRR");
	  	console.log(err);
	  	done(err);
	  }
	});
});

/*
setTimeout(nextTask, 3000);
function nextTask()
{

	DomainCheck
	.findOne({running:true })		
	.exec(function (err, check) {
		if(check){
			var jobmeta =  {
		         title: 'finish him',
		         checkid: check._id,		         
		    };
		    console.log(jobmeta);
			job = jobs.create(FROOTCONFIG.minions.finishhim,jobmeta).priority('normal').save(function(err){			
				
			});
		}else{
			console.log("no more tasks dude");
			setTimeout(nextTask, 1000);
		}
	});
}*/