var http = require('http'),
	exec = require('child_process').exec,
	fs = require('fs'),
	parseString = require('xml2js').parseString;

var imagemagick = require('imagemagick-native');
var fs = require('fs');
var crypto = require('crypto');

var https = require('https'),
    key = 'KEY',
    url = 'URL',
    strategy = 'desktop';

module.exports = {
	getHar:function(id,url,callback){
		var filename =  "/var/www/launcheckv1/temp/"+(this.md5(id+"-"+this.slugifyUrl(url)))+".har";
		var cmd = "phantomjs /var/www/launcheckv1/netsniff.js '"+url+"' > '"+filename+"'"
		var child = exec( cmd,{timeout:18000},function (error, stdout, stderr){
			if(error){
				callback && callback({error:this.EXCEC_ERROR,msg:error},null);
				return;
			}
			/**
			 ***** If we want to parse the har file ******
			var content = fs.readFileSync(filename, 'utf8');
 			var json =  JSON.parse(content);
 			**/

			return callback && callback(null,filename);
		});	
	},
	getErrorsAndConsole:function(id,url,callback){
		var filename =  "/var/www/launcheckv1/temp/"+this.md5(id+"-"+this.slugifyUrl(url))+"-capture-console.json";
		var cmd = "phantomjs /var/www/launcheckv1/extra-capture.js '"+url+"' > '"+filename+"'";
		var child = exec( cmd,{timeout:18000},function (error, stdout, stderr){
			if(error){
				callback && callback({error:this.EXCEC_ERROR,msg:error},null,filename);
				return;
			}
			var str = fs.readFileSync(filename, 'utf8');
 			try{
		      		var json = JSON.parse(str);
		    }
		    catch(e){		      		
		      		callback && callback({error:this.PARSE_ERROR,msg:e},null,filename);
		      		return;
		    }

			return callback && callback(null,json,filename);
		});	
	},
	getGooglePageSpeedDesktop:function(id,url,callback){
		https.get({
		    host: 'www.googleapis.com', 
		    path: '/pagespeedonline/v1/runPagespeed?url=' + encodeURIComponent(url) + 
		          '&key=AIzaSyBNskxT8SAXwaBXslwLZFpstzTB8mNOZB0&strategy='+'desktop'
		    }, function(res) {
		      
		      var str = "";
		      res.on('data', function(d) {
		        str+= d.toString();
		      });
		      res.on('end',function(){
		      	try{
		      		var pagespeed = JSON.parse(str);
		      	}
		      	catch(e){		      		
		      		callback && callback({error:this.PARSE_ERROR,msg:e},null);
		      		return;
		      	}

		      	if(pagespeed.pageStats){
		      		return callback && callback(null,pagespeed);
			    }
			    
			    return callback && callback({error:this.UNKNOWN_ERROR},null);
		      });
		}).on('error', function(e) {
		    return callback && callback({error:this.PARSE_ERROR,msg:e},null);		    
		});		
	},
	getGooglePageSpeedMobile:function(id,url,callback){
		https.get({
		    host: 'www.googleapis.com', 
		    path: '/pagespeedonline/v1/runPagespeed?url=' + encodeURIComponent(url) + 
		          '&key=AIzaSyBNskxT8SAXwaBXslwLZFpstzTB8mNOZB0&strategy='+'mobile'
		    }, function(res) {
		      
		      var str = "";
		      res.on('data', function(d) {
		        str+= d.toString();
		      });
		      res.on('end',function(){
		      	try{
		      		var pagespeed = JSON.parse(str);
		      	}
		      	catch(e){		      		
		      		callback && callback({error:this.PARSE_ERROR,msg:e},null);
		      		return;
		      	}

		      	if(pagespeed.pageStats){
		      		return callback && callback(null,pagespeed);
			    }
			    
			    return callback && callback({error:this.UNKNOWN_ERROR},null);
		      });
		}).on('error', function(e) {
		    return callback && callback({error:this.PARSE_ERROR,msg:e},null);		    
		});		
	},
	getYslowWithHar:function(id,url,harfilename,callback){
		var filename = "/var/www/launcheckv1/temp/"+this.md5(id+"-"+this.slugifyUrl(url))+"-capture-yslow.json"; 
		var cmd = "yslow '"+harfilename +"'> '"+filename+"'";
		var child = exec( cmd,{timeout:18000},function (error, stdout, stderr){
			if(error){
				return callback && callback({error:this.EXCEC_ERROR,msg:error},null,filename);
			}
			var content = fs.readFileSync(filename, 'utf8');
			try{
 				var json =  JSON.parse(content);
				return callback && callback(null,json,filename);
			}catch(e){
				return callback && callback({error:this.PARSE_ERROR},null,filename);
			}
		});	
	},
	getScreenshot:function(id,url,index,callback){
		if(!this.screenshotSizes[index])
		{
			console.log("Out of range: ",index);
			return callback && callback({error:this.EXEC_ERROR},index,null,null,null,null);					    	
		}			
		var slug = this.slugifyUrl(url);
		var cmd =  "phantomjs /var/www/launcheckv1/phantom-capture-s.js '"+url+"' '"+this.md5(id+"-"+slug)+"'  "+index;

		var cucothis = this;
		console.log("pre-exec");
		console.log(cmd);
		try{
  		var child = exec( cmd,{timeout:18000},						  
					  function (error, stdout, stderr) {
						console.log("callbackexec",arguments);

					  	if(error){
					    	//we go for the nex screenshot nevertheless
					    	console.log("exec-error",error);
					    	return callback && callback({error:this.EXEC_ERROR},index,null,null,null,null);					    	
					    } 			
					    var slug = cucothis.slugifyUrl(url);
	    				var filename = cucothis.md5(id+"-"+slug) + cucothis.screenshotSizes[index][0]+"x"+cucothis.screenshotSizes[index][1];
	    				var filepath = '/var/www/launcheckv1/temp/'+filename+'.jpg'; 
	    				try{
							var srcData  = fs.readFileSync('./temp/'+filename+'.jpg');
							var fileStats =  fs.statSync('./temp/'+filename+'.jpg')
						}catch(e){
							console.log("exec-read-error",e);
							return callback && callback({error:this.FILE_NOT_FOUND},index,null,null,null,null);					    	
						}

						var rezisedBuffer = imagemagick.convert({
						    srcData: srcData, // provide a Buffer instance
						    width: 128,
						    height: 128,
						    resizeStyle: "aspectfit",
						    quality: 80,
						    format: 'JPEG'
						});	

						var screenshotData =  imagemagick.identify({srcData: srcData });
						var screenshotThumbData =  imagemagick.identify({srcData: rezisedBuffer });
						var fileSize = fileStats["size"];
						//var key = checkid+"-"+file+".jpg";
						console.log("returning normal");
						return callback && callback(null,index,srcData,rezisedBuffer,screenshotData,screenshotThumbData,fileSize,filename,filepath);


					  }
					);
		}catch(e)
		{
			console.log("error exec",e);
			return callback && callback({error:this.EXEC_ERROR},index,null,null,null,null);
		}

	},
	slugifyUrl:function(text){
	    text=  text.toString().toLowerCase();
	    text = text.replace("http://", "");    
	    text = text.replace(/\//gi, "-");
	    text = text.replace(/[^\._-a-zA-Z0-9,&\s]+/ig, '');  
	    text = text.replace(/\s/gi, "-");
	    text = text.replace(/\&/gi, "-");
	    return text;
	},
	md5: function(str){
	  return crypto.createHash('md5').update(str).digest('hex');
	},
	NOT_FOUND : 9900,
	EXEC_ERROR: 9901,
	PARSE_ERROR:9902,
	UNKNOWN_ERROR: 9903,
	FILE_NOT_FOUND: 9904,
	screenshotSizes : [
        [320,  480],
        [480,  320],        
        [1024, 768],
        [768, 1024],        
        [1440, 900]
        //[640,  960], //2x
        //[640, 1136], //2x
        //[2048,1536], //2x
    ]
};
