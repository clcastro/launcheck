var http = require('follow-redirects').http,
	https = require('follow-redirects').https,
	parseString = require('xml2js').parseString;


module.exports = {
  sitemap : function(sitemaps,limit,urlobj,callback){
  		//[TODO] we have to improve this, by now we find the base sitemap.xml and not the folder sitemap.xml
  		var url = ((urlobj.protocol)?urlobj.protocol:'http:') + "//" + urlobj.hostname + ( (urlobj.port == 80 ||  !urlobj.port )?"":(":"+urlobj.port)  ) + '/sitemap.xml';
		var requester = http;
  		if(urlobj.protocol  == "https:") requester = https;
  		if(sitemaps && sitemaps.length > 0)
  		{
  			var urls = [];

  			function next(sitemaps,index)
  			{
  				console.log("next",sitemaps.length,index);
  				if(index > sitemaps.length-1) 
  				{
  					return callback & callback(null, urls);
  				}
  				var req = http;
  				if(sitemaps[index].indexOf("https://") >= 0){
  					req = https;
  				}
  				console.log("req",sitemaps[index]);

  				
  				req.get( 
  					sitemaps[index], 
			      	function(res) {					      

				      var str = "";
				      res.on('data', function(d) {
				        str+= d.toString();
				      });
				      res.on('end',function(){					      	
				      	if(res.statusCode!=200){
				      		return callback & callback({error:11111},null);		
				      	}

						parseString(str, function (err, result) {
						    if(err)
						    {						    	
						    	return next(sitemaps,index+1);
						    	//return callback & callback({error:11112},null);
						    }
						    	
						    if(result && result.urlset && result.urlset.url)
						    {
						    	urls = urls.concat( result.urlset.url );
						    	if(urls.length >= limit)
						    	{						    		
						    		return callback & callback(null, urls);
						    	}
						    	else
						    	{
						    		next(sitemaps,index+1);
						    	}
						    }
						    else if(result && result.sitemapindex && result.sitemapindex.sitemap)
						    {
						    	for(i=0;i<result.sitemapindex.sitemap.length;i++)
						    	{
						    		sitemaps.push(result.sitemapindex.sitemap[i].loc);
						    	}
						    	next(sitemaps,index+1);						    	
						    }
						    else{					
						    	console.log(result)	;
						    	return callback & callback({error:22222}, null);
						    }
						});					      
				      });
					}).on('error', function(e) {		  
						console.log(5);
					  return callback & callback({error:11111},null);
					});	
  			}
  			next(sitemaps,0);
  		}
  		else{
  			
  			requester.get( url, 
		      function(res) {			

	      		if(res.statusCode!=200 || res.headers['content-type'] !="text/xml" ){
			      		return callback & callback({error:11111},null);		
			      }
		      var str = "";
		      res.on('data', function(d) {
		        str+= d.toString();
		      });
		      res.on('end',function(){					      	
				parseString(str, function (err, result) {
				    if(err){
				    	console.log(url,err,6);
				    	return callback & callback({error:11112},null);
				    }
				    	
				    if(result && result.urlset && result.urlset.url)
				    	return callback & callback(null, result.urlset.url);
				    else
				    	return callback & callback({error:22222}, null);
				});					      
		      });
			}).on('error', function(e) {		  				
			  return callback & callback({error:11111},null);
			});

  		}	

  },
  robots_txt : function(urlobj,callback){
  		var requester = http;
  		if(urlobj.protocol  == "https:") requester = https;
  		requester.get({
		    host: urlobj.hostname + ( (urlobj.port == 80 ||  !urlobj.port )?"":(":"+urlobj.port)  ), 
		    path: '/robots.txt'
		    }, function(res) {					      
		    		if(res.statusCode!=200){
				    	return callback & callback({error:11111},null);		
				    }
				    var str = "";
				    res.on('data', function(d) {
				        str+= d.toString();
				    });
				    res.on('end',function(){	
					    if(res.statusCode != 200 )				      					
				    		callback & callback({error:this.ERROR_NOT_FOUND}, null);				    
					   	else
					   		callback & callback(null, str);				    
					});					      
		      }
		).on('error', function(e) {		  
		  return callback & callback({error:this.ERROR_NOT_FOUND},null);
		});
  
  },
  humans_txt : function(urlobj,callback){
  		var requester = http;
  		if(urlobj.protocol  == "https:") requester = https;
  		requester.get({
		    host: urlobj.hostname + ( (urlobj.port == 80 ||  !urlobj.port )?"":(":"+urlobj.port)  ), 
		    path: '/humans.txt'
		    }, function(res) {	

		    	if(res.statusCode!=200 || res.headers['content-type'] !="text/plain"){
		    		return callback & callback({error:this.ERROR_NOT_FOUND}, null);			
		    	}
			    var str = "";
			    res.on('data', function(d) {
			        str+= d.toString();
			    });
			    res.on('end',function(){	
			    	if(res.statusCode != 200)				      					
				    	return callback & callback({error:this.ERROR_NOT_FOUND}, null);				    
				   	else
				   		return callback & callback(null, str);				    
				});					      
		     
		}).on('error', function(e) {		  
		  return callback & callback({error:this.ERROR_NOT_FOUND},null);
		});
  },
  favicon : function(urlobj,callback){
  		var requester = http;
  		if(urlobj.protocol  == "https:") requester = https;
  		requester.get({
		    host: urlobj.hostname + ( (urlobj.port == 80 ||  !urlobj.port )?"":(":"+urlobj.port)  ), 
		    path: '/favicon.ico'
		    }, function(res) {					      

			    switch(res.statusCode){
			    	case 200:
			    		callback & callback(null, true,res.headers['content-type'] == 'image/x-icon');				    
			    	break;
			    	case 404:
			    	default:
			    		callback & callback({error:this.ERROR_NOT_FOUND}, false);				    
			    	break;
			    }				
			
		     
		}).on('error', function(e) {		  
		  return callback & callback({error:this.ERROR_NOT_FOUND},null);
		});
  },
  remove_www_subdomain : function(urlobj,callback){

  },
  ERROR_NOT_FOUND : 11111,
  ERROR_PARSE : 11112,
  ERROR_UNKNOWN: 22222

};