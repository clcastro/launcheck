var http = require('follow-redirects').http,
	https = require('follow-redirects').https;
var Crawler = require("simplecrawler");
var $ = require('jquery');
var parseString = require('xml2js').parseString;

var logentries = require('node-logentries');
var log = logentries.logger({
  token:'1e8d763f-b943-4c65-9c7b-2bca21a20279'
});

module.exports = {
	crawl:function(url,options,limit,timeout,callback){
		var doCrawl = function(url_a,options_a,limit_a,timeout_a,callback_a){
			var showConsole = options_a.showConsole || false;
			try{
				var scrawler = new Crawler(url_a);
			}catch(e){
				delete scrawler;
				return callback_a(e,null);
			}

			/*scrawler.interval = 1000; // Ten seconds
			scrawler.maxConcurrency = 3;*/
			scrawler.scanSubdomains = true;
			//scrawler.filterByDomain = false;
			scrawler.downloadUnsupported = false;			

			if(options_a && options_a.addUrlsFromSitemaps === true){
				//TODO: When done we add the urls from sitemap
				//scrawler.queueURL(sitemapindexURL);
			}
			
			if(options_a && options_a.fetchConditionRegexp){
				scrawler.addFetchCondition( function(parsedURL) {
		            return !parsedURL.path.match(options_a.fetchConditionRegexp);
		        });
			}
			else{
				scrawler.addFetchCondition( function(parsedURL) {
		            return !parsedURL.path.match(/\.(pdf|png|otf|jpg|gif|jpeg|svg|eot|woff|ttf|htc|js|css)$/i);
		        });
			}
			
			var pageCheckData = [];
			var errorFileData = [];

			

			scrawler			  					  
			  .on("fetchcomplete",function(queueItem, responseBuffer , response ){ 					

			  	var url = queueItem.url;					
				console.log('...',url);
			  	if(showConsole) console.log("[fetchcomplete] ",url);
				if(queueItem.stateData  && queueItem.stateData.contentType &&  queueItem.stateData.contentType.match(/text\/html/gi) ){
					if(pageCheckData.length + 1 > limit){
		            	log.info("[CRAWLER]",url_a,"stopping crawl at the begining, total:",pageCheckData.length," maximum: ",limit);				            	
		            	if(showConsole) console.log("[fetchcomplete][no-continue] ",url);
		            	return;
		            }									

				    var html = responseBuffer.toString(); 
					var data = {};
					try{
				        var $html = $(html);
			    	}catch(e){					    		
			    		log.err("[CRAWLER]",url_a,"Error parsing HTML for jQUERY");
			    		if(showConsole){ console.log("[fetchcomplete][html-error] ",url); console.error(e.stack); }
			    		return;
			    	}

			    	data.html5 = {is: html.indexOf("<!DOCTYPE html>") > -1 };
		            data.title = $html.find("head title").text();                                    //sacamos el titulo
		            data.favicon = $html.find('link[rel~="icon"]').attr("href");                   //sacamos el favicon
		            $aicons = $html.find('link[rel="apple-touch-icon"],link[rel="apple-touch-icon-precomposed"]');
		            data.apple_icon = $aicons.length > 0 ? $($aicons[0]).attr("href"):'';				            				            
		            var gaRegExp = /_gaq.push\(\['_setAccount'\s*,\s*'(UA-\w*-\w)'\]\s*\)\s*;/gi; 
		            var match  = html.match(gaRegExp);
		            var ga = match && match[1];
		            if( ga ) data.ga = ga; 
		            
		      
		            var og = {};
		            var tc = {};
		            var info = {};
		            var seo = {};
		            var mobile = {};
		            var win8 ={};
		            var metas = [];


		            $html.find("head meta").each(function(){
		                 $meta = $(this);
		                 var content = $meta.attr("content");    
		                 var property = $meta.attr("property")+"";
		                 var name = $meta.attr("name")+"";
		                 var equiv = $meta.attr("http-equiv")+"";
		                 metas.push({property:property,name:name,equiv:equiv,content:content});		                 
		                 switch( property.toLowerCase() )
		                 { 		                
		                    case "og:site_name": og.site_name = content; break;  
		                    case "og:title":     og.title = content; break;
		                    case "og:description":     og.description = content; break;
		                    case "og:image":     og.image = content; break;
		                    case "og:type":      og.type = content; break;
		                    case "og:url":       og.url = content; break;
		                    case "fb:admins":    og.fb_admins = content; break;
		                    case "twitter:card":         tc.card = content; break;
		                    case "twitter:url":          tc.url = content; break;
		                    case "twitter:title":        tc.title = content; break;
		                    case "twitter:description":  tc.description = content; break;
		                    case "twitter:image":        tc.image = content; break;
		                    case "twitter:site":         tc.site = content; break;
		                    case "twitter:creator":      tc.creator = content; break;

		                 }

		                 switch( name.toLowerCase() )
		                 {
		                    case "author":       seo.author = content; break;
		                    case "description":  seo.description = content; break;
		                    case "generator":    seo.generator = content; break;
		                    case "keywords":     seo.keywords = content; break;
		                    case "robots":     seo.robots = content; break;
		                    case "generator":     seo.generator = content; break;
		                    case "viewport":             mobile.viewport = content; break;   
		                    case "application-name": win8.applicationName = content; break;
		                    case "msapplication-TileColor": win8.tileColor = content; break;		                    
		                 }

		                 switch( equiv.toLowerCase() )
		                 {
		                    case "refresh":       seo.refresh = content; break;                    
		                 }

		            });
		          				            
					
		            var pagecheck = { mobile:mobile, seo:seo, facebook:og, twitter: tc,metas: metas };

		            pagecheck.url = queueItem.url;
		            pagecheck.httpStatus = queueItem.stateData.code;				            
		            pagecheck.contentLength = queueItem.stateData.contentLength;
		            pagecheck.contentType = queueItem.stateData.contentType;

		            pagecheck.title = data.title;
		            pagecheck.favicon = data.favicon;
		            pagecheck.apple_icon = data.apple_icon;
		            if(data.ga) pagecheck.ga = data.ga;
					
					pagecheck.win8 = win8;
					if(showConsole){ console.log("[fetchcomplete][pushing] ",url,metas.length); }
		            pageCheckData.push(pagecheck);

		            delete responseBuffer; 		            

		            if(pageCheckData.length + 1 > limit_a){
		            	log.notice("[CRAWLER]",url_a,"Limit reached");
		            	if(showConsole){ console.log("[fetchcomplete][limit] ",url,metas.length); }
		            	scrawler.stop();
		            	clearTimeout(this.timeOut);
		            	callback_a(null,{crawler:scrawler, pages: pageCheckData,errors: errorFileData,totalFiles: scrawler.queue.length } );//since complete event is not called when the crawler is stoped
		            	callback_a = null;
		            	delete pageCheckData;
		            	delete errorFileData;
		            	return;
		            }
				}
			  	
			  })
			 .on("fetch404",function( queueItem, response ){  			 	
			  	var checkfile = {};
			  	checkfile.url = queueItem.url;
			  	checkfile.httpStatus = queueItem.stateData.code;			            
	            checkfile.contentLength = queueItem.stateData.contentLength;
	            checkfile.contentType = queueItem.stateData.contentType;	           
	            errorFileData.push(checkfile);			            
	            return true;
			  })
			  .on("fetcherror",function( queueItem, response ){ 			  	
			  	var checkfile = {};
			  	checkfile.url = queueItem.url;
			  	checkfile.httpStatus = queueItem.stateData.code;			            
	            checkfile.contentLength = queueItem.stateData.contentLength;
	            checkfile.contentType = queueItem.stateData.contentType;
	            
	            errorFileData.push(checkfile);

			   })
			  .on("fetchtimeout",function(  queueItem, crawlerTimeoutValue ){			  	  
			  	log.notice("[CRAWLER]",url_a,"Timeout  [",queueItem.url,"]",crawlerTimeoutValue);	  	    	  
			  	if(showConsole){ console.log("[fetchtimeout] ",queueItem.url,crawlerTimeoutValue); }
			   })					   
			  .on("complete",function(){ 
			  	log.notice("[CRAWLER]",url_a,"COMPLETED Total [",pageCheckData.length,"] from Maximum [",limit_a,"]");	  	    	
			  	if(showConsole){ console.log("[complete] "); }
            	scrawler.stop();
            	clearTimeout(this.timeOut);
            	callback_a(null,{crawler:scrawler,pages: pageCheckData,errors: errorFileData,totalFiles: scrawler.queue.length } );//since complete event is not called when the crawler is stoped				
            	callback_a = null;
            	delete pageCheckData;
		        delete errorFileData;
		        return false;
			  })	

			  .on("fetchclienterror",function(queueItem,errorData){ 
			  	log.err("[CRAWLER]",url_a,"fetchclienterror ",errorData,errorData.stack);	  	    	
			  	if(showConsole){ console.log("[fetchclienterror][error] ",url); console.error(errorData.stack); }
			  	
			  	scrawler.stop();
            	clearTimeout(this.timeOut);
            	callback_a({error:"fetchclienterror"},{crawler:scrawler,pages: pageCheckData,errors: errorFileData,totalFiles: scrawler.queue.length } );//since complete event is not called when the crawler is stoped				  					
            	callback_a = null;
            	delete pageCheckData;
		        delete errorFileData;
		        return false;
			   })

 
			  .on("queueerror",function(errorData , URLData){ 			  	
				if(showConsole){ console.log("[queueerror]",errorData); console.error(errorData.stack) }	
			   })

			  .on("queueadd",function(queueItem){ 			  	
				if(showConsole){ console.log("[queueadd] ",queueItem.url); }	
			   })
				.on("fetchredirect",function(queueItem, parsedURL, response){ 			  	
				if(showConsole){ console.log("[fetchredirect]",queueItem.url,parsedURL);  }	
			   })

			  .on("crawlstart",function(){  if(showConsole){ console.log("[crawlstart] "); }	 })
			  .on("fetchstart",function(){  if(showConsole){ console.log("[fetchstart] "); }	 })
			  //.on("queueduplicate",function(){  if(showConsole){ console.log("[queueduplicate] "); }	 })
			  .on("fetchheaders",function(){  if(showConsole){ console.log("[fetchheaders] "); }	 })
			  .on("discoverycomplete",function(queueItem, resources ){  
			  		if(showConsole){ console.log("[discoverycomplete] ",queueItem.url); }	


			  })
			  .on("fetchdataerror",function(queueItem, response){ 			  	
			  	if(showConsole){ console.log("[fetchdataerror] ",queueItem.url); }					
			   });			  
			  
			  
			  
			  
			  
			return {
				timeOut : null, 
				//if the crawl is not complete yet when the timeout is called, we say that there was an error
				areWeCrawledYet:function(){
					log.err("[CRAWLER]",url_a,"Crawl timeout OBTAINED [",pageCheckData.length,"] From maximum [",limit_a,"]");
					if(showConsole){ console.log("[timeout] "); }	
					scrawler.stop();
	            	clearTimeout(this.timeOut);
	            	if(callback_a) callback_a({error:"timeouterror"},{crawler:scrawler,pages: pageCheckData,errors: errorFileData,totalFiles: scrawler.queue.length } );//since complete event is not called when the crawler is stoped				  					
	            	
	            	delete pageCheckData;
		            delete errorFileData;
		            delete scrawler;
		            return false;
				},
				start:function(){
					//Tick tock. the clock is ticking
					this.timeOut = setTimeout(this.areWeCrawledYet,timeout_a);					
					log.notice("[CRAWLER]",url_a," START  ===============");	  	  
					if(showConsole){ console.log("[start] ",url_a); }	
					scrawler.start();
				}
			}
		};
		var dc = new doCrawl(url,options,limit,timeout,callback);
		dc.start();		
	}
}