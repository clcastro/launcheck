var mongoose = require('mongoose');
var Domain = require('./models/domain');
var DomainCheck = require('./models/domaincheck');
var DomainCheckFile = require('./models/domaincheckfile');
var PageCheck = require('./models/pagecheck');
var Task = require('./models/task');
var check4 = require('./lib/check4');
var kue = require('kue'),
    url = require('url'),
    robots = require('robots'),
    jobs = kue.createQueue();

var ocrawler = require('./lib/ocrawler');

var FROOTCONFIG = require('config');
mongoose.connect(FROOTCONFIG.db.mongo);

var http = require('http');
var Crawler = require("simplecrawler");
var $ = require('jquery');
var parseString = require('xml2js').parseString;
var limit = 8;


jobs.process(FROOTCONFIG.minions.crawler, 2,function(job, done){
	console.log("Processing ",job.data.title,job.data.checkid);
	DomainCheck
	.findOne({_id:job.data.checkid})
	.populate("_domain")
	.exec(function (err, dcheck) {
		if (err) return console.warn("error getting domain ",err);		
		console.log("dcheck",dcheck);
		var urlobj = {hostname:dcheck._domain[0].hostname,port:dcheck._domain[0].port,path:dcheck._domain[0].path};  
		var parser ;
		check4.robots_txt( urlobj, function(err,robot){	
/////////////////////////
			if(!err){
				parser = new robots.RobotsParser();
				parser.parse(robot.split("\n"));
			}
			//parser.getSitemaps(function(sitemaps) {    	  			
			// [OK] this is weird because we would suposed to use the sitemap that we found in robots,
			// but in google this create a HUGUE MESS, so by now we are disabling this, and just getting the default sitemap.xml
  			check4.sitemap([],limit, urlobj, function(errSitemap,urls){
/////////////////////////
				if(errSitemap){
  					
  				}  				
				check4.humans_txt( urlobj, function(errHumans,humans){  
/////////////////////////
					check4.favicon( urlobj, function(errFavicon,isfavicon,isicon){  
/////////////////////////					
						console.log("i",humans,isfavicon,isicon);
						console.log("crawling");

						var url = urlobj.hostname + ( (urlobj.port == 80 ||  !urlobj.port )?"":(":"+urlobj.port)  ) ;
						ocrawler.crawl(url,{showConsole:false},limit,FROOTCONFIG.minions.crawlerTimeoutx10*1000,function(err,results){									
							console.log("crawled");
							console.log(results.pages);
							var pageIDS = [];							
						
							dcheck.pendingCrawl = false;						
							dcheck.totalFiles = results.totalFiles;
							dcheck.totalPages = (results.pages && results.pages > 0) ? results.pages.length : 0;
							dcheck.totalPagesChecked = (results.pages && results.pages > 0) ? results.pages.length : 0;
							dcheck.brokenLinksNumber = (results.errors && results.errors > 0) ? results.pages.length : 0;
							dcheck.hasSitemap = urls !== null; 														
							dcheck.hasRobotsTXT = robot !== null;
							if(dcheck.hasRobotsTXT)
								dcheck.robotsTXT = robot;

							dcheck.hasHumansTXT = humans !== null;
							if(dcheck.hasHumansTXT)
								dcheck.humansTXT = humans;
							dcheck.hasFavicon = isfavicon;
							dcheck.faviconIsIcon = isicon;						    
					        dcheck.crawlCompletedOn = new Date();							

					        if(err && ((results.pages && results.pages.length == 0) || !results.pages ) ){
					        	console.log("yes there where errors")
					        	dcheck.crawlSuccesfull = false;
					        	dcheck.crawlErrors = [err];
					        	dcheck.sucessfull = false;
					        	dcheck.pendingValidation = false;
    							dcheck.pendingBrowsing = false;
    							dcheck.pendingCuconsole = false;
    							dcheck.running = false;
    							dcheck.completed = new Date();
    							dcheck.save(function(err){
    								done();														
    							});
					        }else{
					        	console.log("maybe errors but with pages")
					        	if(err){
					        		console.log("Tes with errors");
					        		dcheck.crawlErrors = [err];
					        		dcheck.crawlSuccesfull = false;
					        	}

					        	var pages = results.pages;
					        	for(i=0;i<pages.length;i++){
					        		pages[i]._domaincheck = dcheck._id;
					        	}

								PageCheck.create(pages, function (err) {
								console.log("creating pages");
								  if (err){  console.log("Error Saving PageCheck",err); }

								  for(i=1; i<arguments.length; i++){
								  	dcheck.pages.push(arguments[i]._id);
								  	pageIDS.push(arguments[i]._id);
								  }
								  console.log("creating error files");
								  DomainCheckFile.create(results.errors, function (err) {
								  		if (err){ 
								  			console.log("Error Saving DomainCheckFile",err);
								  		}
										for(i=1; i<arguments.length; i++){
										  	dcheck.files.push(arguments[i]._id);
										}	



																				
								  		dcheck.save(function(err){
											var tasking = [];											
											for (var i in FROOTCONFIG.minionsData.general.tasksperpage) 
								  			{
											  tarray = FROOTCONFIG.minionsData.general.tasksperpage[i];
											  for(var j = 0; j < tarray.length; j++){

											  	for( k = 0 ; k < pageIDS.length ;  k++ ){
												  	tasking.push(
												  					{
												  						"name": tarray[j].name,
												  						"args": tarray[j].args,
												  						"checkid": dcheck._id,
												  						"pageid": pageIDS[k],
												  						"created": new Date(),
												  						"status": "created"
												  					}
												  				);
											    }
											  }
											};

											Task.create(tasking, function (err) {
												console.log("sending the minions horde");
									  			var priority = 'low'; 
	                                            if(job.data.uid) priority = 'medium';
									  			for(i=0;i<pageIDS.length;  i++){
									  				console.log("sending ",pageIDS[i]);
									  				jobs.create(FROOTCONFIG.minions.cuconsole,{pageid:pageIDS[i] ,checkid:dcheck._id,title:"cuconsole "+pageIDS[i]}).priority(priority).save(function(){});
									  				jobs.create(FROOTCONFIG.minions.pagespeed,{pageid:pageIDS[i] ,checkid:dcheck._id,title:"pagespeed "+pageIDS[i]}).priority(priority).save(function(){});
													jobs.create(FROOTCONFIG.minions.browser,{pageid:pageIDS[i] ,checkid:dcheck._id,title:"browsing "+pageIDS[i]}).priority(priority).save(function(){});
													jobs.create(FROOTCONFIG.minions.validator,{pageid:pageIDS[i] ,checkid:dcheck._id,title:"validating "+pageIDS[i]}).priority(priority).save(function(){});
									  			}									  		
									  			delete results.crawler;
												delete results.pages;
												delete results.errors;

												job.progress("100", "100");							
												done();														
											});
								  			
									  	});
								   });
								});	
							}						

						});
/////////////////////////
					});
/////////////////////////				
				});
/////////////////////////  				
			});			
/////////////////////////			
  		});			
	});
});
/*
setTimeout(function(){
	var jobmeta =  {
         title: 'Crawl fitmob.com',
         checkid: '52e58b95c718b73d2f000005'
    };
	job = jobs.create(FROOTCONFIG.minions.crawler,jobmeta).priority('normal').save(function(err){
        	if (err) return log.critical("Error saving the kue ("+FROOTCONFIG.minions.crawler+") on (/check) [url|",baseurl,"]",err); 
	});

}, 3000);
*/